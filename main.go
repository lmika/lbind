package main

import (
	"flag"
	"io"
	"log"
	"os"

	"bitbucket.org/lmika/lbind/binds"
	"bitbucket.org/lmika/lbind/goluagen"
	"bitbucket.org/lmika/lbind/luadoc"
	"bitbucket.org/lmika/lbind/parser"
)

func main() {
	flagDoc := flag.Bool("d", false, "generate documentation")
	flagOutput := flag.String("o", "", "target file")

	flag.Parse()

	inFilename := ""
	if flag.NArg() >= 1 {
		inFilename = flag.Arg(0)
	}

	bindings, err := parseBindings(inFilename)
	if err != nil {
		log.Fatal(err)
	}

	err = writeToOutput(*flagOutput, func(w io.Writer) error {
		if *flagDoc {
			return luadoc.Generate(w, bindings)
		} else {
			return goluagen.Generate(w, bindings)
		}
	})

	if err != nil {
		log.Fatal(err)
	}
}

// parse bindings from a file.  If inFile is empty, this will read from stdin.
func parseBindings(inFilename string) (*binds.Bindings, error) {
	var inFile io.Reader = os.Stdin
	if inFilename != "" {
		file, err := os.Open(inFilename)
		if err != nil {
			return nil, err
		}
		defer file.Close()

		inFile = file
	}

	return parser.Parse(inFile)
}

// writeToOutput sets up the file and calls fn to write the result to output.
func writeToOutput(outFilename string, fn func(w io.Writer) error) error {
	var outFile io.Writer = os.Stdout
	if outFilename != "" {
		file, err := os.Create(outFilename)
		if err != nil {
			return err
		}
		defer file.Close()

		outFile = file
	}

	return fn(outFile)
}

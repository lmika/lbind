lbind - Lua Binding Generator
=============================

A utility for generating the binding between Lua and Go.  Works with
[golua](github.com/Shopify/go-lua).

Example Spec File
-----------------

**NOTE**: Spec file is still in development.  Here's an example:

```
package mybindings

// Regular expression module.
module re {

    // Returns true if the supplied string matches the given pattern.
    // Errors cause a Lua error and are not returned from calling the function.
    func match(pattern: string, s: string): (bool, error) = regex.MatchString
}

// Simple testing module
module test {
    // Module variables
    var test: *testing.T

    // Bind to a method from a module variable
    func log(m: string) = $test.Log
}
```

See [Spec File](_docs/SpecFile.md) for more info on the spec file.
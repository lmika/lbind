Spec File
=========

The spec file details the mapping between Go types and functions and how they are
to be represented in Lua.

## Top Level Elements

TODO

## Modules

TODO

## Function Mappings

TODO

## Type Mappings

Type mappings define how types in Go are to be represented in Lua.  The general format
for a type mapping is:

    typemap <GoType> = <LuaType>

where _GoType_ is the Go type to expose in Lua scripts, and _LuaType_ is how values of
that type will be represented in Lua.  Lbind will generate the necessary code to marshal
and unmarshal values of this GoType between Go and Lua.

There are various mapping techniques that can be applied to GoTypes.  This will depend on
the GoType itself, and what forms 

### Primitive Type Mapping

Primitive type mappings map a Go type directly to a primitive Lua type.  Type mappings
of this form can be specified using the following format:

    typemap <GoType> = <PrimitiveLuaType>

where _PrimitiveLuaType_ is either:

- `string`
- `int`
- `bool`
- `float32` (TEMPORARY, will be removed)
- `float64` (TEMPORARY, will eventually be changed to `float`)

Without any additional options, the primitive Lua type will be casted directly to the Go type
and visa-versa.

### Array Type Mapping

TODO

    typemap <GoSliceType> = array

### Table Type Mapping

TODO

    typemap <GoStructType> = table {
        <LuaFieldName> = <GoFieldName>: <GoType>
    }

### Function Type Mapping

TODO

    typemap <GoFunctionType> = func ( <args> ): <returns>

### Class Type Mapping

TODO

    typemap <GoStructType> = class {
        <FunctionMembers>
    }
package main

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"

	"bitbucket.org/lmika/lbind/goluagen"
	"bitbucket.org/lmika/lbind/parser"
)

func TestMapToNative(t *testing.T) {
	testFiles := []string{
		"_testdata/simple1.txt",

		"_testdata/primtypes1.txt",
		"_testdata/primtypes2.txt",
		"_testdata/primtypes3.txt",
	}

	for _, testFile := range testFiles {
		t.Run(testFile, func(t *testing.T) {
			fileContent, err := ioutil.ReadFile(testFile)
			if err != nil {
				t.Errorf("Cannot load test file: %v", err)
				return
			}

			td, err := readTestData(bytes.NewReader(fileContent))
			if err != nil {
				t.Errorf("Cannot read test data from test file: %v", err)
				return
			}

			td.CompileAndRun(t)
		})
	}
}

type testData struct {
	Definition string
	GoCode     string
	LuaScript  string
	Expected   string
}

// readTestData from a reader
func readTestData(r io.Reader) (td testData, err error) {
	scanner := bufio.NewScanner(r)
	segment := 0
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "===" {
			segment++
		} else {
			switch segment {
			case 0:
				td.Definition += scanner.Text() + "\n"
			case 1:
				td.GoCode += scanner.Text() + "\n"
			case 2:
				td.LuaScript += scanner.Text() + "\n"
			case 3:
				td.Expected += scanner.Text() + "\n"
			}
		}
	}
	err = scanner.Err()
	return td, err
}

func (td testData) CompileAndRun(t *testing.T) {
	tmpDir, err := ioutil.TempDir("", "tmplbind")
	if err != nil {
		t.Fatalf("Cannot create temporary dir: %v", err)
		return
	}
	t.Logf("Dir = %v", tmpDir)

	bindings, err := parser.Parse(strings.NewReader(td.Definition))
	if err != nil {
		t.Errorf("Cannot parse definition: %v", err)
		return
	}

	// Write bindings
	if err = writeToOutput(filepath.Join(tmpDir, "bindings.go"), func(w io.Writer) error {
		return goluagen.Generate(w, bindings)
	}); err != nil {
		t.Errorf("Cannot generate bindings: %v", err)
		return
	}

	// Write Go code
	if err = ioutil.WriteFile(filepath.Join(tmpDir, "gocode.go"), []byte(td.GoCode), 0644); err != nil {
		t.Errorf("Cannot write gocode: %v", err)
		return
	}

	// Write main code
	if err = ioutil.WriteFile(filepath.Join(tmpDir, "main.go"), []byte(mainCode), 0644); err != nil {
		t.Errorf("Cannot write main code: %v", err)
		return
	}

	// Run the code
	cmd := exec.Command("go", "run",
		filepath.Join(tmpDir, "main.go"),
		filepath.Join(tmpDir, "bindings.go"),
		filepath.Join(tmpDir, "gocode.go"),
		"-c", td.LuaScript,
	)
	out, err := cmd.Output()
	if err != nil {
		if exitError, isExitError := err.(*exec.ExitError); isExitError {
			t.Errorf("Connot run 'go run': %v\n\tStderr: %v", err, string(exitError.Stderr))
		} else {
			t.Errorf("Connot run 'go run': %v", err)
		}
		return
	}

	if string(out) != td.Expected {
		t.Errorf("Actual does not match expected:\nExp: %v\nAct: %v", td.Expected, string(out))
	}

	// Only delete the temporary directory if the test succeeded
	os.RemoveAll(tmpDir)

}

const mainCode = `package main

import (
	golua "github.com/Shopify/go-lua"
	"log"
	"flag"
)

func main() {
	script := flag.String("c", "", "Lua code")
	flag.Parse()

	l := golua.NewState()
	golua.Require(l, "_G", golua.BaseOpen, true)
	golua.Require(l, "main", new(mainModule).Register, true)
	
	if err := golua.DoString(l, *script); err != nil {
		log.Fatal(err)
	}
}
`

package luadoc

import (
	"fmt"
	"strings"

	"bitbucket.org/lmika/lbind/binds"
)

// treeBuilder is responsible for building the document tree
type treeBuilder struct {
	bindings *binds.Bindings
}

// build builds the document tree
func (tb *treeBuilder) build() (*docTree, error) {
	tree := &docTree{}

	for _, m := range tb.bindings.Modules {
		// Add the module and top-level functions
		err := tb.buildModule(tree, m)
		if err != nil {
			return nil, err
		}

	}

	return tree, nil
}

// buildModule builds a module and adds the top-level functions
func (tb *treeBuilder) buildModule(tree *docTree, m *binds.Module) error {
	mod := &docItem{
		ID:          m.Name,
		Name:        m.Name,
		Title:       "Module " + m.Name,
		Description: m.Remarks,
	}

	// Top-level functions
	if err := tb.addFunctionItems(mod, m.Name, m.Functions); err != nil {
		return err
	}

	tree.AddItem(mod)

	// Add any "top-level" classes
	for _, typeMapping := range m.TypeMappings {
		switch tm := typeMapping.ScriptType.(type) {
		case *binds.ClassType:
			if err := tb.buildClass(tree, typeMapping, tm); err != nil {
				return err
			}
		}
	}

	return nil
}

// buildClass builds a module and adds the top-level functions
func (tb *treeBuilder) buildClass(tree *docTree, tm *binds.TypeMapping, ct *binds.ClassType) error {
	className := ct.Name
	classDoc := &docItem{
		ID:          "class-" + className,
		Name:        className,
		Title:       "Class " + className,
		Description: tm.Remarks,
	}

	// Add function methods
	if err := tb.addFunctionItems(classDoc, className, ct.Methods); err != nil {
		return err
	}

	tree.AddItem(classDoc)
	return nil
}

// addFunctionItems
func (tb *treeBuilder) addFunctionItems(mod *docItem, prefix string, fns []*binds.FunctionModuleItem) error {
	for _, fn := range fns {
		fnItem, err := tb.buildFnItem(prefix, fn)
		if err != nil {
			return err
		}

		mod.Items = append(mod.Items, fnItem)
	}
	return nil
}

// buildFn builds a module item using a function
func (tb *treeBuilder) buildFnItem(prefix string, fn *binds.FunctionModuleItem) (*docItem, error) {
	classMethod := false
	methodName := prefix + "." + fn.Name

	// Set the syntax
	argNames := make([]string, 0)
	for _, arg := range fn.Args {
		if arg.IsClassThis {
			classMethod = true
		} else {
			argNames = append(argNames, arg.Name)
		}
	}

	if classMethod {
		methodName = prefix + ":" + fn.Name
	}

	syntax := fmt.Sprintf("%s(%s)", methodName, strings.Join(argNames, ", "))

	docItem := &docItem{
		Name:        fn.Name,
		Title:       methodName,
		Syntax:      syntax,
		Description: fn.Remarks,
	}

	return docItem, nil
}

package luadoc

import (
	"fmt"
	"io"
	"strings"
	"text/template"

	"bitbucket.org/lmika/lbind/binds"
)

// Generate lua documentation.
func Generate(w io.Writer, bindings *binds.Bindings) error {
	tree, err := (&treeBuilder{bindings}).build()
	if err != nil {
		return err
	}

	t, err := template.New("itemTemplate").Parse(itemTemplate)
	if err != nil {
		return fmt.Errorf("Cannot compile template: %v", err)
	}

	for _, item := range tree.Items {
		if err := renderItem(w, t, 0, item); err != nil {
			return err
		}
	}

	return nil
}

func renderItem(w io.Writer, template *template.Template, rank int, item *docItem) error {
	itemTitle := strings.Repeat("#", rank+1) + " " + item.Title

	if err := template.Execute(w, map[string]interface{}{
		"Title": itemTitle,
		"Item":  item,
	}); err != nil {
		return fmt.Errorf("Error rendering template: %v", err)
	}

	for _, subitem := range item.Items {
		if err := renderItem(w, template, rank+1, subitem); err != nil {
			return err
		}
	}

	return nil
}

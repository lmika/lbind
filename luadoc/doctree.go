package luadoc

// docTree is the top-level tree in the documentation.
type docTree struct {
	Items []*docItem
}

func (dt *docTree) AddItem(m *docItem) {
	dt.Items = append(dt.Items, m)
}

// docItem is an item in a module.  These would be used for top-level
// types like classes, etc.
type docItem struct {
	ID          string
	Name        string
	Title       string
	Syntax      string
	Description string

	// Any subitems
	Items []*docItem
}

func (dt *docItem) AddItem(m *docItem) {
	dt.Items = append(dt.Items, m)
}

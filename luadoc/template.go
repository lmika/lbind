package luadoc

/*
const luadocTemplate = `{{range .Modules}}
# {{.Title}}

{{.Description}}
{{range .Items}}
## {{.Title}}

	{{.Syntax}}

{{.Description}}
{{end}}
{{end}}
`
*/

const itemTemplate = `
{{.Title}}
{{if .Item.Syntax}}
	{{.Item.Syntax}}{{end}}

{{.Item.Description}}
`

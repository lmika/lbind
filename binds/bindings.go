package binds

type Bindings struct {
	Package string

	// Import aliases
	Imports map[string]string
	Modules []*Module
}

type Module struct {
	Name         string
	GoName       string
	Remarks      string
	Fields       []*ModuleField
	TypeMappings []*TypeMapping
	Functions    []*FunctionModuleItem
}

type ModuleField struct {
	Name string
	Type Type
}

type FunctionModuleItem struct {
	Name       string
	GoFunction GoFunctionRef
	Args       []*FunctionArg
	Rets       []*FunctionArg
	Remarks    string
}

type FunctionArg struct {
	Name        string
	Type        Type
	IsClassThis bool
}

type FunctionRet struct {
	Name string
	Type Type
}

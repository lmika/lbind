package binds

import (
	"fmt"
	"path"

	"github.com/dave/jennifer/jen"
)

// Type is a reference to a Go type.
type Type interface {
	// String returns a human readable representation of this type
	String() string

	// Simple name returns the name of this type suitable for identifiers
	SimpleIdentifier() string

	// ToJenId returns a jen statement of this type
	ToJenId() *jen.Statement
}

// Builtin types reference a builtin Go types.
type BuiltinType struct {
	name    string
	genStmt *jen.Statement
}

func (bt BuiltinType) String() string {
	return bt.name
}

func (bt BuiltinType) SimpleIdentifier() string {
	return bt.name
}

func (bt BuiltinType) ToJenId() *jen.Statement {
	return bt.genStmt
}

// Local type references a type located in the current package
type LocalType string

func (lt LocalType) String() string {
	return string(lt)
}

func (lt LocalType) SimpleIdentifier() string {
	return string(lt)
}

func (lt LocalType) ToJenId() *jen.Statement {
	return jen.Id(string(lt))
}

// Qualfied type references a type of another package
type QualifiedType struct {
	Package string
	Name    string
}

func (qt QualifiedType) String() string {
	return fmt.Sprintf("\"%s\".%s", qt.Package, qt.Name)
}

func (qt QualifiedType) SimpleIdentifier() string {
	packageBaseName := path.Base(qt.Package)
	return fmt.Sprintf("%s%s", packageBaseName, qt.Name)
}

func (qt QualifiedType) ToJenId() *jen.Statement {
	return jen.Qual(qt.Package, qt.Name)
}

// SliceType is a slice of another type
type SliceType struct {
	ElemType Type
}

func (st SliceType) String() string {
	return fmt.Sprintf("[]%s", st.ElemType)
}

func (st SliceType) SimpleIdentifier() string {
	return fmt.Sprintf("%sSlice", st.ElemType.SimpleIdentifier())
}

func (st SliceType) ToJenId() *jen.Statement {
	return jen.Index().Add(st.ElemType.ToJenId())
}

// PointerType is a pointer of another type
type PointerType struct {
	DerefType Type
}

func (pt PointerType) String() string {
	return fmt.Sprintf("*%s", pt.DerefType)
}

func (st PointerType) SimpleIdentifier() string {
	return fmt.Sprintf("PointerTo%s", st.DerefType.SimpleIdentifier())
}

func (pt PointerType) ToJenId() *jen.Statement {
	return jen.Op("*").Add(pt.DerefType.ToJenId())
}

var (
	StringType  = BuiltinType{"string", jen.String()}
	IntType     = BuiltinType{"int", jen.Int()}
	BoolType    = BuiltinType{"bool", jen.Bool()}
	Float32Type = BuiltinType{"float32", jen.Float32()}
	Float64Type = BuiltinType{"float64", jen.Float64()}
	ErrorType   = BuiltinType{"error", jen.Error()}
)

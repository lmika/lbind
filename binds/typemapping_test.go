package binds_test

import (
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/lmika/lbind/binds"
	"bitbucket.org/lmika/lbind/parser"
	"github.com/seanpont/assert"
)

func TestMapToPrimitive(t *testing.T) {
	assert := assert.Assert(t)

	b, err := parseBindings(`package test module test { typemap GoStringType = string }`)

	assert.Nil(err)
	assert.NotNil(b)

	module := b.Modules[0]
	typeMapping := module.TypeMappings[0]

	assert.Equal(typeMapping.GoType, binds.LocalType("GoStringType"))
	assert.Equal(typeMapping.ScriptType, binds.NativeScriptType("string"))
}

func parseBindings(def string) (*binds.Bindings, error) {
	b, err := parser.Parse(strings.NewReader(def))
	if err != nil {
		return nil, fmt.Errorf("Parse error: %v", err)
	}

	return b, nil
}

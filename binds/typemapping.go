package binds

// TypeMapping is a mapping from a GoType to a script type
type TypeMapping struct {
	GoType     Type
	ScriptType ScriptType
	Remarks    string
}

// ScriptType is the base interface for script types
type ScriptType interface {

	// String returns the script type as a string
	String() string
}

// NativeScriptType is a reference to a type supported by the target scripting
// language.
type NativeScriptType string

func (pst NativeScriptType) String() string {
	return string(pst)
}

// OpaqueScriptType is an opaque script type.  This will pass the Go type
// as a opaque value through the script environment.  In Lua, this will
// effectively be userdata.
type OpaqueScriptType struct {
}

func (ost OpaqueScriptType) String() string {
	return "opaque"
}

// ArrayScriptType maps to an array.
type ArrayScriptType struct {
}

func (ost ArrayScriptType) String() string {
	return "array"
}

// TableScriptType maps a script table (like a Lua table or a JS object) to
// a GoType.  The keys of the table will map to fields of the specific Go type.
type TableScriptType struct {
	FieldMappings []*FieldMapping
}

func (tst *TableScriptType) String() string {
	return "table"
}

// Sets a field of a struct.
// TODO: Have additional options for field mappings (e.g. getters/setters)
type FieldMapping struct {
	ScriptField string
	GoField     string
	GoFieldType Type
}

// ClassType simulates a class.
type ClassType struct {
	// Name of the class.
	Name string

	// The methods of the class
	Methods []*FunctionModuleItem
}

func (ct *ClassType) String() string {
	return "class"
}

// FnScriptType maps a script function to a GoType.
type FnScriptType struct {
	Args []*FunctionArg
	Rets []*FunctionArg
}

func (fst *FnScriptType) String() string {
	return "func"
}

package binds

import (
	"github.com/dave/jennifer/jen"
)

// FnGenEnv contains information about the environment used for producing
// the JenId for the function reference
type FnGenEnv interface {
	// ModuleVar returns the name of the module type receiver
	ModuleReceiver() string

	// Returns true if this variable is a local variable
	IsLocalVar(name string) bool
}

// Types of Go function bindings
type GoFunctionRef interface {

	// Returns a jen statement of this function.  This takes a variable
	// referencing the module type.
	ToJenId(env FnGenEnv) *jen.Statement
}

type LocalGoFunctionRef string

func (lf LocalGoFunctionRef) ToJenId(env FnGenEnv) *jen.Statement {
	return jen.Id(string(lf))
}

// A fully qualified Go function ref
type QualifiedGoFunctionRef struct {

	// The package of the Go function
	Pkg string

	// The name of the Go function
	Name string
}

func (qf QualifiedGoFunctionRef) ToJenId(env FnGenEnv) *jen.Statement {
	return jen.Qual(qf.Pkg, qf.Name)
}

// A function hanging off a module field
type ModuleVarFunctionRef struct {

	// The module field name
	VarName string

	// The name of the Go function
	Name string
}

func (mf ModuleVarFunctionRef) ToJenId(env FnGenEnv) *jen.Statement {
	if env.IsLocalVar(mf.VarName) {
		return jen.Id(mf.VarName).Dot(mf.Name)
	} else {
		return jen.Id(env.ModuleReceiver()).Dot(mf.VarName).Dot(mf.Name)
	}
}

package goluagen

import "github.com/dave/jennifer/jen"

// typeMapping is the top-level interface for all type mappings.
type typeMapping interface {
	// ReadFromStack returns the code necessary to read a value of this particular
	// type at stack position stackPos into the variable
	//
	// TODO: Change return type to *jen.Stmt
	ReadFromStack(fw *fnWriter, stackPos int, toVar string) error

	// PushToStack generates the code necessary to push a variable
	// of this type to the stack.
	PushToStack(fw *fnWriter, fromVar string) error
}

// raisableTypeMapping are for types that raise an error.  Unless they implement
// typeMapping, they cannot be read from or push to the opstack.
type raisableTypeMapping interface {
	typeMapping

	// RaiseError generates the code necessary to raise an error using
	// a variable of this Go type.  The type must be of type "error".
	//
	// TODO: Break this out into a separate interface
	RaiseError(fw *fnWriter, fromVar string) error
}

// registrableTypeMapping is a type mapping that adds some registration code to
// the module registration implementation.
type registrableTypeMapping interface {
	typeMapping

	// Register the type in the module registration
	RegisterType(fw *fnWriter) error
}

// generatableTypeMapping is a type mapping that generates some functions.
type generatableTypeMapping interface {
	typeMapping

	// GenerateFnsForType generates functions for this type
	GenerateFnsForType(ctx *genContext, f *jen.File) error
}

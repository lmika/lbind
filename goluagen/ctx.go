package goluagen

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
	"bitbucket.org/lmika/lbind/luagen/ir"
)

// genContext contains some utilities useful for code generation
type genContext struct {
	// Various names
	LuaPackage string
	StateType  string
	ContextVar string

	NewLibraryFn         string
	RegistryFunctionType string

	NativeTypeNames map[string]ir.LuaType
	TypeMapping     map[binds.Type]typeMapping
}

func newGenContext() *genContext {
	return &genContext{
		LuaPackage:           "github.com/Shopify/go-lua",
		StateType:            "State",
		ContextVar:           "l",
		NewLibraryFn:         "NewLibrary",
		RegistryFunctionType: "RegistryFunction",

		NativeTypeNames: map[string]ir.LuaType{
			"string": ir.StringLuaType,
			"int":    ir.IntLuaType,
			"bool":   ir.BoolLuaType,
			"float":  ir.Float64LuaType,
		},

		// Preload the type mappings with the builtin types
		TypeMapping: map[binds.Type]typeMapping{
			binds.StringType: &builtinTypeMapping{
				irType: ir.StringLuaType,
			},
			binds.IntType: &builtinTypeMapping{
				irType: ir.IntLuaType,
			},
			binds.BoolType: &builtinTypeMapping{
				irType: ir.BoolLuaType,
			},
			binds.Float32Type: &builtinTypeMapping{
				irType: ir.Float32LuaType,
			},
			binds.Float64Type: &builtinTypeMapping{
				irType: ir.Float64LuaType,
			},
			binds.ErrorType: &errorTypeMapping{},
		},
	}
}

// LookupType returns the typeMapping for the passed in type.  If no type is found,
// returns an error.
func (ctx *genContext) LookupType(t binds.Type) (typeMapping, error) {
	if tm, hasTm := ctx.TypeMapping[t]; hasTm {
		return tm, nil
	}

	return nil, fmt.Errorf("No mapping for type: %v", t)
}

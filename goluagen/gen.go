package goluagen

import (
	"io"
	"time"

	"bitbucket.org/lmika/lbind/binds"

	"github.com/dave/jennifer/jen"
)

type duktapeGenerator struct {
	bindings *binds.Bindings
	modules  []*moduleItem
}

// Generate generates Duktape bindings for the set of bindings and writes it to
// the given writer.
func Generate(w io.Writer, bindings *binds.Bindings) error {
	dg := &duktapeGenerator{bindings: bindings}

	for _, module := range bindings.Modules {
		dg.modules = append(dg.modules, buildModuleItem(module))
	}

	return dg.generate(w)
}

func (dg *duktapeGenerator) generate(w io.Writer) error {
	f := jen.NewFile(dg.bindings.Package)

	f.Comment("Generated using lbind on " + time.Now().String() + ".")
	f.Comment("Re-generating bindings will erase changes.  DO NOT MODIFY.")
	f.Comment("See: https://bitbucket.org/lmika/lbind/")
	f.Line()
	f.Line()

	ctx := newGenContext()

	// Module types
	for _, module := range dg.modules {
		if err := module.GenType(ctx, f); err != nil {
			return err
		}
	}

	// Type mappings
	for _, module := range dg.modules {
		if err := module.GenTypeMappings(ctx, f); err != nil {
			return err
		}
	}

	// Module binding functions
	for _, module := range dg.modules {
		if err := module.GenImpl(ctx, f); err != nil {
			return err
		}
	}

	// Module registration
	for _, module := range dg.modules {
		if err := module.GenRegistration(ctx, f); err != nil {
			return err
		}
	}

	// Generate the shared utilities
	if err := dg.generateSharedUtils(ctx, f); err != nil {
		return err
	}

	return f.Render(w)
}

// generateSharedUtils generate shared utilities
func (dg *duktapeGenerator) generateSharedUtils(ctx *genContext, f *jen.File) error {
	// Shared sequence generator
	f.Var().Id("lb_uidseq").Int()

	f.Func().Id("lb_newUid").Params().String().Block(
		jen.Id("lb_uidseq").Op("++"),
		jen.Return(jen.Qual("fmt", "Sprintf").Call(
			jen.Lit("lb_uidseq_"+dg.bindings.Package+"_%d"),
			jen.Id("lb_uidseq"),
		)),
	)

	return nil
}

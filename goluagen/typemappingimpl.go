package goluagen

import (
	"errors"
	"log"

	"bitbucket.org/lmika/lbind/binds"
	"bitbucket.org/lmika/lbind/luagen/goluatarget"
	"bitbucket.org/lmika/lbind/luagen/ir"

	"github.com/dave/jennifer/jen"
)

// builtinTypeMapping is for a built in type mapping
type builtinTypeMapping struct {
	// Functions to read from, and push to stack
	irType ir.LuaType
}

func (btm *builtinTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	fw.add(jen.Do(func(group *jen.Statement) {
		gc := &ir.GenCtx{G: group, NewVarFn: fw.newVar}
		if err := goluatarget.New().Generate(ir.Block{
			ir.ToStackOp{ToVar: toVar, Pos: stackPos, Type: btm.irType},
		}, gc); err != nil {
			log.Fatal(err)
		}
	}))
	return nil
}

func (btm *builtinTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	fw.add(jen.Do(func(group *jen.Statement) {
		gc := &ir.GenCtx{G: group, NewVarFn: fw.newVar}
		if err := goluatarget.New().Generate(ir.Block{
			ir.PushStackOp{FromVar: fromVar, Type: btm.irType},
		}, gc); err != nil {
			log.Fatal(err)
		}
	}))
	return nil
}

type builtinWithCastTypeMapping struct {
	irType ir.LuaType
	goType binds.Type
}

func (btm *builtinWithCastTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	fw.add(jen.Do(func(group *jen.Statement) {
		gc := &ir.GenCtx{G: group, NewVarFn: fw.newVar}
		luaVar := gc.NewVar()

		if err := goluatarget.New().Generate(ir.Block{
			ir.ToStackOp{ToVar: luaVar, Pos: stackPos, Type: btm.irType},
			ir.CastToGoTypeOp{ToVar: toVar, FromVar: luaVar, GoType: btm.goType},
		}, gc); err != nil {
			log.Fatal(err)
		}
	}))
	return nil
}

func (btm *builtinWithCastTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	fw.add(jen.Do(func(group *jen.Statement) {
		gc := &ir.GenCtx{G: group, NewVarFn: fw.newVar}
		if err := goluatarget.New().Generate(ir.Block{
			ir.PushStackOp{FromVar: fromVar, Type: btm.irType, MustCast: true},
		}, gc); err != nil {
			log.Fatal(err)
		}
	}))
	return nil
}

// errorTypeMapping maps an error type
type errorTypeMapping struct{}

func (btm errorTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	return errors.New("Cannot read error type from stack (yet)")
}

func (btm errorTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	return errors.New("Cannot push error type to stack (yet)")
}

func (etm errorTypeMapping) RaiseError(fw *fnWriter, fromVar string) error {
	fw.add(jen.If(jen.Id(fromVar).Op("!=").Nil()).Block(
		jen.Id(fw.ctxVar()).Dot("PushString").Call(jen.Id(fromVar).Dot("Error").Call()),
		jen.Id(fw.ctxVar()).Dot("Error").Call(),
		jen.Return(jen.Lit(0)),
	))

	return nil
}

// opaqueTypeMapping is for a type that the Lua code has no access to.
type opaqueTypeMapping struct {
	Type binds.Type
}

func (tm *opaqueTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	fw.add(jen.Id(toVar).Op(":=").Id(fw.ctxVar()).Dot("ToUserData").Call(jen.Lit(stackPos)).Assert(tm.Type.ToJenId()))

	return nil
}

func (tm *opaqueTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	fw.add(jen.Id(fw.ctxVar()).Dot("PushUserData").Call(jen.Id(fromVar)))

	return nil
}

// arrayTypeMapping is for a type that is mapped to a Lua "array"
type arrayTypeMapping struct {
	Type        binds.Type
	ElementType binds.Type
}

func (at *arrayTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	lenVal := fw.newVar()

	fw.add(jen.Id(toVar).Op(":=").Make(at.Type.ToJenId(), jen.Lit(0)))
	fw.add(jen.Id(lenVal).Op(":=").Id(fw.ctxVar()).Dot("RawLength").Call(jen.Lit(stackPos)))

	blockFw := fw.fork()
	holdVar := blockFw.newVar()
	blockFw.add(jen.Id(fw.ctxVar()).Dot("RawGetInt").Call(jen.Lit(stackPos), jen.Id("i")))
	blockFw.addReadFromStack(at.ElementType, -1, holdVar)
	blockFw.add(jen.Id(toVar).Op("=").Append(jen.Id(toVar), jen.Id(holdVar)))
	blockFw.add(jen.Id(fw.ctxVar()).Dot("Pop").Call(jen.Lit(1)))

	blockCode, err := blockFw.generatedCode()
	if err != nil {
		return err
	}

	fw.add(jen.For(
		jen.Id("i").Op(":=").Lit(1),
		jen.Id("i").Op("<=").Id(lenVal),
		jen.Id("i").Op("++"),
	).Block(blockCode...))

	return nil
}

func (at *arrayTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	blockFw := fw.fork()
	blockFw.addPushToStack(at.ElementType, "v")
	blockFw.add(jen.Id(fw.ctxVar()).Dot("RawSetInt").Call(jen.Lit(-2), jen.Id("i").Op("+").Lit(1)))

	blockCode, err := blockFw.generatedCode()
	if err != nil {
		return err
	}

	fw.add(jen.Id(fw.ctxVar()).Dot("CreateTable").Call(jen.Len(jen.Id(fromVar)), jen.Lit(0)))
	fw.add(jen.For(jen.Id("i").Op(",").Id("v").Op(":=").Range().Call(jen.Id(fromVar))).Block(blockCode...))

	return nil
}

package goluagen

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
	"github.com/dave/jennifer/jen"
)

type fnTypeMapping struct {
	Type            binds.Type
	Target          *binds.FnScriptType
	moduleItem      *moduleItem
	wrapperTypeName string
}

func newFnTypeMapping(bindType binds.Type, target *binds.FnScriptType, moduleItem *moduleItem) *fnTypeMapping {
	return &fnTypeMapping{
		Type:            bindType,
		Target:          target,
		moduleItem:      moduleItem,
		wrapperTypeName: "fnType" + bindType.SimpleIdentifier(),
	}
}

// ReadFromStack returns the code necessary to read a value of this particular
// type at stack position stackPos into the variable
func (fn *fnTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	// Long term function
	regIndexNameVar := fw.newVar()
	fw.add(jen.Id(regIndexNameVar).Op(":=").Id("lb_newUid").Call())

	fw.add(jen.Id(fw.ctxVar()).Dot("PushValue").Call(jen.Lit(stackPos)))
	fw.add(jen.Id(fw.ctxVar()).Dot("SetField").Call(
		jen.Qual(fw.ctx.LuaPackage, "RegistryIndex"),
		jen.Id(regIndexNameVar),
	))

	fnTypeVar := fw.newVar()
	fw.add(jen.Id(fnTypeVar).Op(":=").Op("&").Id(fn.wrapperTypeName).Values(
		jen.Id(fw.ctxVar()),
		jen.Id(regIndexNameVar),
	))

	fw.add(jen.Id(toVar).Op(":=").Id(fnTypeVar).Dot("call"))

	fw.add(jen.Qual("runtime", "SetFinalizer").Call(
		jen.Id(fnTypeVar),
		jen.Func().Params(jen.Id("_fn").Op("*").Id(fn.wrapperTypeName)).Block(
			jen.Id("_fn").Dot("release").Call(),
		),
	))
	return nil
}

// PushToStack generates the code necessary to push a variable
// of this type to the stack.
func (fn *fnTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	// TODO
	fw.add(jen.Comment("TODO: Push function onto stack"))
	fw.add(jen.Op("_").Op("=").Id(fromVar))
	fw.add(jen.Id(fw.ctxVar()).Dot("PushNil").Call())
	return nil
}

func (fn *fnTypeMapping) genCallArgsAndRets() (args []jen.Code, rets []jen.Code) {
	args = make([]jen.Code, len(fn.Target.Args))
	for i, a := range fn.Target.Args {
		args[i] = jen.Id(fmt.Sprintf("a%d", i)).Add(a.Type.ToJenId())
	}

	rets = make([]jen.Code, len(fn.Target.Rets))
	for i, a := range fn.Target.Rets {
		//rets[i] = jen.Id(fmt.Sprintf("r%d", i)).Add(a.Type.ToJenId())
		rets[i] = a.Type.ToJenId()
	}

	return
}

// Generate the call function
func (fn *fnTypeMapping) genCallFn(ctx *genContext) ([]jen.Code, error) {
	fw := &fnWriter{ctx: ctx}

	// Set the context variable
	fw.add(jen.Id(fw.ctxVar()).Op(":=").Id("f").Dot("l"))

	// Push the function
	fw.add(jen.Id(fw.ctxVar()).Dot("Field").Call(jen.Qual(ctx.LuaPackage, "RegistryIndex"), jen.Id("f").Dot("rn")))

	// Push the arguments
	for i, a := range fn.Target.Args {
		fw.addPushToStack(a.Type, fmt.Sprintf("a%d", i))
	}

	// Make the call
	fw.add(jen.Id(fw.ctxVar()).Dot("ProtectedCall").Call(
		jen.Lit(len(fn.Target.Args)),
		jen.Lit(len(fn.Target.Rets)),
		jen.Lit(0),
	))
	// TODO: handle errors

	// Get the results
	retVars := make([]jen.Code, len(fn.Target.Rets))
	for i, r := range fn.Target.Rets {
		retName := fmt.Sprintf("r%d", i)
		retVars[i] = jen.Id(retName)
		fw.addReadFromStack(r.Type, -len(fn.Target.Rets)+i, retName)
	}

	// Pop the results from the stack
	fw.add(jen.Id(fw.ctxVar()).Dot("Pop").Call(jen.Lit(len(fn.Target.Rets))))

	// Add the return
	fw.add(jen.Return(retVars...))

	return fw.generatedCode()
}

// Generate additional code for the function type
func (fn *fnTypeMapping) GenerateFnsForType(ctx *genContext, f *jen.File) error {
	args, rets := fn.genCallArgsAndRets()

	f.Type().Id(fn.wrapperTypeName).Struct(
		jen.Id("l").Op("*").Qual(ctx.LuaPackage, ctx.StateType), // Lua state
		jen.Id("rn").String(),                                   // Registry name holding function
	)

	callBody, err := fn.genCallFn(ctx)
	if err != nil {
		return err
	}

	f.Func().Params(
		jen.Id("f").Op("*").Id(fn.wrapperTypeName),
	).Id("call").Params(args...).Params(rets...).Block(callBody...)

	f.Func().Params(
		jen.Id("f").Op("*").Id(fn.wrapperTypeName),
	).Id("release").Params().Block(
		//jen.Qual("log", "Println").Call(jen.Lit("Running finalizer for function:"), jen.Id("f").Dot("rn")),
		jen.Id("f").Dot("l").Dot("PushNil").Call(),
		jen.Id("f").Dot("l").Dot("SetField").Call(jen.Qual(ctx.LuaPackage, "RegistryIndex"), jen.Id("f").Dot("rn")),
	)

	return nil
}

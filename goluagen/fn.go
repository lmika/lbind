package goluagen

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
	"github.com/dave/jennifer/jen"
)

// fnItem is a declared FunctionModuleItem
type fnItem struct {
	Fn     *binds.FunctionModuleItem
	Module *binds.Module

	callVars  []*fnVar
	retVars   []*fnVar
	throwVars []*fnVar
	varNames  map[string]*fnVar

	register     bool
	qualifier    string
	receiverName string
	receiverType string
}

// determine the call variables, return variables and throw variables
func (fi *fnItem) determineVars(ctx *genContext) error {
	fi.varNames = make(map[string]*fnVar)

	// Call arguments
	fi.callVars = make([]*fnVar, 0)
	for i, arg := range fi.Fn.Args {
		varName := arg.Name
		if varName == "" {
			varName = fmt.Sprintf("arg%d", i)
		}

		mappedType, err := ctx.LookupType(arg.Type)
		if err != nil {
			return fmt.Errorf("cannot map type of %s: %v", varName, err)
		}

		v := &fnVar{varName, arg.Type, mappedType}
		fi.callVars = append(fi.callVars, v)
		fi.varNames[varName] = v
	}

	// Return arguments
	fi.retVars = make([]*fnVar, 0)
	for i, ret := range fi.Fn.Rets {
		varName := ret.Name
		if varName == "" {
			varName = fmt.Sprintf("ret%d", i)
		}

		mappedType, err := ctx.LookupType(ret.Type)
		if err != nil {
			return fmt.Errorf("cannot map type of %s: %v", varName, err)
		}

		v := &fnVar{varName, ret.Type, mappedType}
		fi.retVars = append(fi.retVars, v)
		fi.varNames[varName] = v
	}

	// Handle throw args
	if len(fi.retVars) > 0 {
		lastRetVar := fi.retVars[len(fi.retVars)-1]
		if _, isRaisableType := lastRetVar.typeMap.(raisableTypeMapping); isRaisableType {
			fi.throwVars = []*fnVar{lastRetVar}
			fi.retVars = fi.retVars[:len(fi.retVars)-1]
		}
	}

	return nil
}

// Generate the item implementation
func (fi *fnItem) GenImpl(ctx *genContext, f *jen.File) error {

	// Determine the variables within this function
	if err := fi.determineVars(ctx); err != nil {
		return err
	}

	// Get variables
	callArgs, returnVars := fi.getArgsAndRets()

	fnWriter := &fnWriter{ctx: ctx}

	// Handle parameters
	fi.genArgUnmarshal(fnWriter)

	// If the first caller is a class receiver, do not include it in the call args.
	// HACK - this should be cleaned up a little
	if len(fi.Fn.Args) > 0 && fi.Fn.Args[0].IsClassThis {
		if varFnRef, isVarFnRef := fi.Fn.GoFunction.(binds.ModuleVarFunctionRef); isVarFnRef {
			if varFnRef.VarName == fi.Fn.Args[0].Name {
				callArgs = callArgs[1:]
			}
		}
	}

	fnGenEnv := fnItemEnv{fi}
	if len(returnVars) > 0 {
		fnWriter.add(jen.List(returnVars...).Op(":=").Add(fi.Fn.GoFunction.ToJenId(fnGenEnv)).Call(callArgs...))
	} else {
		fnWriter.add(fi.Fn.GoFunction.ToJenId(fnGenEnv).Call(callArgs...))
	}

	// Return return count
	fi.genRetMarshal(fnWriter)
	fnWriter.add(jen.Return(jen.Lit(len(fi.retVars))))

	body, err := fnWriter.generatedCode()
	if err != nil {
		return err
	}

	f.Func().Params(
		jen.Id(fi.receiverName).Id(fi.receiverType),
	).Id(fi.fnName()).
		Params(jen.Id(ctx.ContextVar).Op("*").Qual(ctx.LuaPackage, ctx.StateType)).
		Int().
		Block(body...)
	return nil
}

func (fi *fnItem) getArgsAndRets() (callArgs []jen.Code, retArgs []jen.Code) {
	callArgs = make([]jen.Code, len(fi.callVars))
	for i, a := range fi.callVars {
		callArgs[i] = a.ToGenCode()
	}

	retArgs = make([]jen.Code, len(fi.retVars)+len(fi.throwVars))
	for i, r := range fi.retVars {
		retArgs[i] = r.ToGenCode()
	}
	for i, t := range fi.throwVars {
		retArgs[len(fi.retVars)+i] = t.ToGenCode()
	}

	return
}

// Generate the code to unmarshal the arguments
func (fi *fnItem) genArgUnmarshal(fw *fnWriter) { //(callArgs []jen.Code) {
	for i, arg := range fi.callVars {
		fw.addReadFromStack(arg.varType, i+1, arg.varName)
	}
}

// Generate the code to marshal the return values
func (fi *fnItem) genRetMarshal(fw *fnWriter) { //(retWriter *fnWriter, retVars []jen.Code, retCount int) {
	for _, throwVar := range fi.throwVars {
		fw.addRaiseError(throwVar.varType, throwVar.varName)
	}

	for _, ret := range fi.retVars {
		fw.addPushToStack(ret.varType, ret.varName)
	}
}

// Generate the registration.  This will generate it using RawSetString
func (fi *fnItem) GenRegistration(modVar string, receiverVar string) ([]jen.Code, error) {
	return []jen.Code{
		jen.Id(modVar).Dot("RawSetString").Call(
			jen.Lit(fi.Fn.Name),                  // Function name
			jen.Id(receiverVar).Dot(fi.fnName()), // Go function implementation
		),
	}, nil
}

func (fi *fnItem) GenRegistryFnLiteral(receiverVar string) jen.Code {
	return jen.Values(
		jen.Lit(fi.Fn.Name),                  // Function name
		jen.Id(receiverVar).Dot(fi.fnName()), // Go function implementation
	)
}

// The name of the generated function
func (fi *fnItem) fnName() string {
	name := fi.Fn.Name
	return "lbfn_" + fi.Module.Name + fi.qualifier + "_" + name
}

// A function variable
type fnVar struct {
	varName string
	varType binds.Type
	typeMap typeMapping
}

func (fv fnVar) ToGenCode() jen.Code {
	return jen.Id(fv.varName)
}

// fnItemEnv is an implementation of binds.FnGenEnv
type fnItemEnv struct {
	fi *fnItem
}

// ModuleVar returns the name of the module type receiver
func (fe fnItemEnv) ModuleReceiver() string {
	return fe.fi.receiverName
}

// Returns true if this variable is a local variable
func (fe fnItemEnv) IsLocalVar(name string) bool {
	_, localVar := fe.fi.varNames[name]
	return localVar
}

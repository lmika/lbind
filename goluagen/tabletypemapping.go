package goluagen

import (
	"bitbucket.org/lmika/lbind/binds"
	"github.com/dave/jennifer/jen"
)

// tableTypeMapping is for a type that is mapped to the fields of a Lua table
type tableTypeMapping struct {
	Type          binds.Type
	FieldMappings []*binds.FieldMapping
	toFnName      string
	pushfnName    string
}

func newTableTypeMapping(fromType binds.Type, fieldMapping []*binds.FieldMapping) *tableTypeMapping {
	return &tableTypeMapping{
		Type:          fromType,
		FieldMappings: fieldMapping,
		toFnName:      "to" + fromType.SimpleIdentifier(),
		pushfnName:    "push" + fromType.SimpleIdentifier(),
	}
}

func (tt *tableTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	fw.add(jen.Id(toVar).Op(",").Id("_").Op(":=").Id(tt.toFnName).Call(
		jen.Id(fw.ctxVar()), jen.Lit(stackPos)))

	return nil
}

func (tt *tableTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	fw.add(jen.Id(tt.pushfnName).Call(jen.Id(fw.ctxVar()), jen.Id(fromVar)))
	return nil
}

// Generate additional code for the function type
func (tt *tableTypeMapping) GenerateFnsForType(ctx *genContext, f *jen.File) error {
	if err := tt.generateToTypeFn(ctx, f); err != nil {
		return err
	}

	return tt.generatePushTypeFn(ctx, f)
}

func (tt *tableTypeMapping) generateToTypeFn(ctx *genContext, f *jen.File) error {
	// Generate the toFunction
	fw := &fnWriter{ctx: ctx}

	toVarName := "res"
	stackPosVarName := "stackPos"

	// If it's a pointer type, call new() to get a new heap allocated value.
	// TODO: add support for custom constructors
	if ptrType, isPtrType := tt.Type.(binds.PointerType); isPtrType {
		fw.add(jen.Id(toVarName).Op(":=").New(ptrType.DerefType.ToJenId()))
	} else {
		fw.add(jen.Var().Id(toVarName).Add(tt.Type.ToJenId()))
	}

	for _, fieldMapping := range tt.FieldMappings {
		fieldVarName := fw.newVar()

		fw.add(jen.Id(fw.ctxVar()).Dot("Field").Call(
			jen.Id(stackPosVarName),
			jen.Lit(fieldMapping.ScriptField),
		))
		fw.addReadFromStack(fieldMapping.GoFieldType, -1, fieldVarName)
		fw.add(jen.Id(toVarName).Dot(fieldMapping.GoField).Op("=").Id(fieldVarName))
		fw.add(jen.Id(fw.ctxVar()).Dot("Pop").Call(jen.Lit(1)))
	}
	fw.add(jen.Return(jen.Id(toVarName), jen.True()))

	code, err := fw.generatedCode()
	if err != nil {
		return err
	}

	f.Func().Id(tt.toFnName).Params(
		jen.Id("l").Op("*").Qual(ctx.LuaPackage, ctx.StateType),
		jen.Id(stackPosVarName).Int(),
	).Params(tt.Type.ToJenId(), jen.Bool()).Block(code...)

	return nil
}

// Generate additional code for the function type
func (tt *tableTypeMapping) generatePushTypeFn(ctx *genContext, f *jen.File) error {
	// Generate the toFunction
	fw := &fnWriter{ctx: ctx}

	fromVarName := "v"

	fw.add(jen.Id(fw.ctxVar()).Dot("CreateTable").Call(jen.Lit(0), jen.Lit(len(tt.FieldMappings))))

	for _, fieldMapping := range tt.FieldMappings {
		fieldVarName := fw.newVar()
		fw.add(jen.Id(fieldVarName).Op(":=").Id(fromVarName).Dot(fieldMapping.GoField))
		fw.addPushToStack(fieldMapping.GoFieldType, fieldVarName)

		fw.add(jen.Id(fw.ctxVar()).Dot("SetField").Call(
			jen.Lit(-2),
			jen.Lit(fieldMapping.ScriptField),
		))
	}

	code, err := fw.generatedCode()
	if err != nil {
		return err
	}

	f.Func().Id(tt.pushfnName).Params(
		jen.Id("l").Op("*").Qual(ctx.LuaPackage, ctx.StateType),
		jen.Id(fromVarName).Add(tt.Type.ToJenId()),
	).Block(code...)

	return nil
}

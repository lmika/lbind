package goluagen

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"

	"github.com/dave/jennifer/jen"
)

// fnWriter is used to write the body of functions.  It also provides some other
// utilities like generating variables and holding errors.
type fnWriter struct {
	ctx       *genContext
	code      []jen.Code
	err       error
	varPrefix string
	varCnt    int
}

// add adds a line of code
func (fw *fnWriter) add(code ...jen.Code) {
	fw.code = append(fw.code, code...)
}

// fork creates a new fnWriter using the configuration of this fnWriter
// The code is not copied.
func (fw *fnWriter) fork() *fnWriter {
	return &fnWriter{
		ctx:       fw.ctx,
		varCnt:    fw.varCnt,
		varPrefix: "x", // TODO: Need some other way of allocating variable numbers
		err:       nil,
	}
}

// addFromWriter adds the code written to the other writer.
func (fw *fnWriter) addFromWriter(other *fnWriter) {
	if fw.err != nil {
		return
	}
	fw.err = other.err
	fw.code = append(fw.code, other.code...)
}

// ctxVar returns the context variable as a jen.ID
func (fw *fnWriter) ctxVar() string {
	return fw.ctx.ContextVar
}

// newVar returns a new variable with a unique name
func (fw *fnWriter) newVar() string {
	fw.varCnt++
	return fmt.Sprintf("x%s%d", fw.varPrefix, fw.varCnt)
}

// generatedCode returns the generated code and the error
func (fw *fnWriter) generatedCode() ([]jen.Code, error) {
	return fw.code, fw.err
}

// addReadFromStack add the code which will read a value of the specific type from the stack.
// This will set the internal error flag if an error is encountered.
func (fw *fnWriter) addReadFromStack(argType binds.Type, stackPos int, varName string) {
	if fw.err != nil {
		return
	}

	tm, err := fw.ctx.LookupType(argType)
	if err != nil {
		fw.err = err
		return
	}

	if err := tm.ReadFromStack(fw, stackPos, varName); err != nil {
		fw.err = err
		return
	}
}

// addPushToStack adds the code which will push a value of the specific type to the stack.
// This will set the internal error flag if an error is encountered.
func (fw *fnWriter) addPushToStack(argType binds.Type, fromVarName string) {
	if fw.err != nil {
		return
	}

	tm, err := fw.ctx.LookupType(argType)
	if err != nil {
		fw.err = err
		return
	}

	if err := tm.PushToStack(fw, fromVarName); err != nil {
		fw.err = err
		return
	}
}

// addRaiseError adds the code which will throw an error.
// This will set the internal error flag if an error is encountered.
func (fw *fnWriter) addRaiseError(argType binds.Type, fromVarName string) {
	if fw.err != nil {
		return
	}

	tm, err := fw.ctx.LookupType(argType)
	if err != nil {
		fw.err = err
		return
	}

	errRet, isErrRet := tm.(raisableTypeMapping)
	if !isErrRet {
		fw.err = fmt.Errorf("%s is not a raisable type", argType.String())
		return
	}

	if err := errRet.RaiseError(fw, fromVarName); err != nil {
		fw.err = err
		return
	}
}

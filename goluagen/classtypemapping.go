package goluagen

import (
	"bitbucket.org/lmika/lbind/binds"
	"github.com/dave/jennifer/jen"
)

// classTypeMapping is for a type that is exposed as a "class" in lua.
type classTypeMapping struct {
	Type   binds.Type
	Target *binds.ClassType

	moduleItem        *moduleItem
	metatableName     string
	methodMissingName string
	methodMapping     map[*binds.FunctionModuleItem]*fnItem
}

func newClassTypeMapping(mi *moduleItem, goType binds.Type, classType *binds.ClassType) *classTypeMapping {

	ctm := &classTypeMapping{
		Type:              goType,
		Target:            classType,
		moduleItem:        mi,
		metatableName:     "cls." + goType.String(),
		methodMissingName: "methodMissingOf" + goType.SimpleIdentifier(),
		methodMapping:     make(map[*binds.FunctionModuleItem]*fnItem),
	}

	// Add the class methods to the module functions
	for _, method := range classType.Methods {
		ctm.methodMapping[method] = mi.addFunctionMapping(mi.Module, method, false, "_clsmethod")
	}

	return ctm
}

func (cm *classTypeMapping) ReadFromStack(fw *fnWriter, stackPos int, toVar string) error {
	fw.add(jen.Id(toVar).Op(":=").Id(fw.ctxVar()).Dot("ToUserData").Call(jen.Lit(stackPos)).Assert(cm.Type.ToJenId()))

	return nil
}

func (cm *classTypeMapping) PushToStack(fw *fnWriter, fromVar string) error {
	fw.add(jen.If(jen.Id(fromVar).Op("!=").Nil().BlockFunc(func(g *jen.Group) {
		g.Id(fw.ctxVar()).Dot("PushUserData").Call(jen.Id(fromVar))
		g.Qual(fw.ctx.LuaPackage, "SetMetaTableNamed").Call(jen.Id(fw.ctxVar()), jen.Lit(cm.metatableName))
	}).Else().BlockFunc(func(g *jen.Group) {
		g.Id(fw.ctxVar()).Dot("PushNil").Call()
	})))

	return nil
}

// Register the type in the module registration
func (cm *classTypeMapping) RegisterType(fw *fnWriter) error {
	hasMethodMissing := false

	fw.add(jen.Comment("Register class type"))

	fw.add(jen.Qual(fw.ctx.LuaPackage, "NewMetaTable").Call(jen.Id(fw.ctxVar()), jen.Lit(cm.metatableName)))

	// Add all the methods to the metadata
	for _, method := range cm.Target.Methods {
		if method.Name == "__keymissing" {
			hasMethodMissing = true
		}
		implFnName := cm.methodMapping[method].fnName()
		fw.add(jen.Id(fw.ctxVar()).Dot("PushGoFunction").Call(jen.Id(cm.moduleItem.receiverName).Dot(implFnName)))
		fw.add(jen.Id(fw.ctxVar()).Dot("SetField").Call(jen.Lit(-2), jen.Lit(method.Name)))
	}

	// If there is a method missing, push the custom __index lookup function.
	if hasMethodMissing {
		fw.add(jen.Id(fw.ctxVar()).Dot("PushGoFunction").Call(jen.Id(cm.methodMissingName)))
	} else {
		fw.add(jen.Id(fw.ctxVar()).Dot("PushValue").Call(jen.Lit(-1)))
	}
	fw.add(jen.Id(fw.ctxVar()).Dot("SetField").Call(jen.Lit(-2), jen.Lit("__index")))
	fw.add(jen.Id(fw.ctxVar()).Dot("Pop").Call(jen.Lit(1)))
	fw.add(jen.Line())

	return nil
}

// Register the type in the module registration
func (cm *classTypeMapping) GenerateFnsForType(ctx *genContext, f *jen.File) error {
	f.Func().Id(cm.methodMissingName).Params(
		jen.Id("l").Op("*").Qual(ctx.LuaPackage, ctx.StateType),
	).Int().BlockFunc(func(b *jen.Group) {
		// [table, key]
		b.Qual(ctx.LuaPackage, "NewMetaTable").Call(jen.Id("l"), jen.Lit(cm.metatableName))
		// [table, key, mt]
		b.Id("l").Dot("PushValue").Call(jen.Lit(2))
		// [table, key, mt, key]
		b.Id("l").Dot("RawGet").Call(jen.Lit(-2))
		// [table, key, mt(key)]
		b.If(jen.Op("!").Id("l").Dot("IsNil").Call(jen.Lit(-1))).BlockFunc(func(t *jen.Group) {
			t.Return(jen.Lit(1))
		})
		// [table, key, mt(key)]
		b.Id("l").Dot("Pop").Call(jen.Lit(1))
		// [table, key]
		b.Qual(ctx.LuaPackage, "NewMetaTable").Call(jen.Id("l"), jen.Lit(cm.metatableName))
		// [table, key, mt]
		b.Id("l").Dot("Field").Call(jen.Lit(-1), jen.Lit("__keymissing"))
		// [table, key, mt(__methodmissing)]
		b.Id("l").Dot("PushValue").Call(jen.Lit(1))
		b.Id("l").Dot("PushValue").Call(jen.Lit(2))
		// [table, key, mt(__methodmissing), table, key]
		b.Id("l").Dot("Call").Call(jen.Lit(2), jen.Lit(1))
		// [table, key, res]
		b.Return(jen.Lit(1))
	})

	return nil
}

package goluagen

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
	"github.com/dave/jennifer/jen"
)

// moduleItem maps to a module
type moduleItem struct {
	Module *binds.Module

	typeName           string
	receiverName       string
	receiverType       string
	registrationFnName string
	fnItems            []*fnItem
	moduleTypes        []typeMapping
}

func buildModuleItem(module *binds.Module) *moduleItem {
	mi := &moduleItem{Module: module}
	mi.typeName = module.GoName
	mi.receiverName = "module"
	mi.receiverType = "*" + mi.typeName
	mi.registrationFnName = "Register"

	// Generate function
	mi.fnItems = make([]*fnItem, 0)
	for _, fn := range module.Functions {
		mi.addFunctionMapping(module, fn, true, "")
	}

	return mi
}

// Adds a function mapping to to the list of module functions
func (mi *moduleItem) addFunctionMapping(module *binds.Module, fn *binds.FunctionModuleItem, register bool, qualifier string) *fnItem {
	fnMap := &fnItem{
		Fn:           fn,
		Module:       module,
		register:     register,
		qualifier:    qualifier,
		receiverName: mi.receiverName,
		receiverType: mi.receiverType,
	}
	mi.fnItems = append(mi.fnItems, fnMap)
	return fnMap
}

// Register type mappings
func (mi *moduleItem) GenTypeMappings(ctx *genContext, f *jen.File) error {
	for _, tm := range mi.Module.TypeMappings {
		var typeMapping typeMapping

		switch target := tm.ScriptType.(type) {
		case binds.OpaqueScriptType:
			typeMapping = &opaqueTypeMapping{tm.GoType}
		case *binds.TableScriptType:
			typeMapping = newTableTypeMapping(tm.GoType, target.FieldMappings)
		case *binds.ClassType:
			typeMapping = newClassTypeMapping(mi, tm.GoType, target)
		case *binds.FnScriptType:
			typeMapping = newFnTypeMapping(tm.GoType, target, mi)
		case *binds.ArrayScriptType:
			if sliceType, isSliceType := tm.GoType.(binds.SliceType); isSliceType {
				typeMapping = &arrayTypeMapping{tm.GoType, sliceType.ElemType}
			} else {
				return fmt.Errorf("Array type mapping must be a slice: %v", tm.GoType)
			}
		case binds.NativeScriptType:
			// Locate the native type
			nativeType, hasNativeType := ctx.NativeTypeNames[string(target)]
			if !hasNativeType {
				return fmt.Errorf("Type '%v' is not a native Lua type", string(target))
			}

			typeMapping = &builtinWithCastTypeMapping{
				irType: nativeType,
				goType: tm.GoType,
			}
		default:
			return fmt.Errorf("Unrecognised target for type mapping: %v, mapped to %v", tm.GoType, target)
		}

		// Assign the type to the module, but make it available to all other modules.
		mi.moduleTypes = append(mi.moduleTypes, typeMapping)
		ctx.TypeMapping[tm.GoType] = typeMapping
	}

	return nil
}

// Generate the module type definition
func (mi *moduleItem) GenType(ctx *genContext, f *jen.File) error {
	fieldCode := []jen.Code{
	//jen.Id("lb_uidseq").Int(),
	}

	// Module fields
	for _, field := range mi.Module.Fields {
		fieldCode = append(fieldCode, jen.Id(field.Name).Add(field.Type.ToJenId()))
	}

	f.Type().Id(mi.typeName).Struct(fieldCode...)

	// Utility functions

	// utility function to generate a new unique ID
	// TODO: make threadsafe
	/*
		f.Func().Params(
			jen.Id("m").Op("*").Id(mi.typeName),
		).Id("newUid").Params().String().Block(
			jen.Id("m").Dot("lb_uidseq").Op("++"),
			jen.Return(jen.Qual("fmt", "Sprintf").Call(
				jen.Lit("_seq_"+mi.typeName+"_%d"),
				jen.Id("m").Dot("lb_uidseq"),
			)),
		)
	*/

	return nil
}

// Generate the module implementations
func (mi *moduleItem) GenImpl(ctx *genContext, f *jen.File) error {
	// Generate types that require custom code
	for _, mt := range mi.moduleTypes {
		if gtm, isGtm := mt.(generatableTypeMapping); isGtm {
			if err := gtm.GenerateFnsForType(ctx, f); err != nil {
				return err
			}
		}
	}

	// Generate functions
	for _, fi := range mi.fnItems {
		if err := fi.GenImpl(ctx, f); err != nil {
			return err
		}
	}

	return nil
}

// Generate the module registration function
func (mi *moduleItem) GenRegistration(ctx *genContext, f *jen.File) error {

	fnWriter := &fnWriter{ctx: ctx}

	// Register all module type mappings that need to add registration code
	for _, moduleTypes := range mi.moduleTypes {
		if registrableType, isRegistrable := moduleTypes.(registrableTypeMapping); isRegistrable {
			registrableType.RegisterType(fnWriter)
		}
	}

	fnMappings := make([]jen.Code, 0)
	for _, fi := range mi.fnItems {
		if fi.register {
			fnMappings = append(fnMappings, fi.GenRegistryFnLiteral(mi.receiverName))
		}
	}

	regFns := jen.Index().Qual(ctx.LuaPackage, ctx.RegistryFunctionType).Values(fnMappings...)

	fnWriter.add(jen.Qual(ctx.LuaPackage, ctx.NewLibraryFn).Call(jen.Id(ctx.ContextVar), regFns))
	fnWriter.add(jen.Return(jen.Lit(1)))

	stmts, err := fnWriter.generatedCode()
	if err != nil {
		return err
	}

	// Wrap it in a function call
	f.Func().Params(
		jen.Id(mi.receiverName).Id(mi.receiverType),
	).Id(mi.registrationFnName).Params(
		jen.Id(ctx.ContextVar).Op("*").Qual(ctx.LuaPackage, ctx.StateType),
	).Int().Block(stmts...)

	return nil
}

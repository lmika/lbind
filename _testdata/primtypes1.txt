package main

module main {
    typemap prefixString = string
    
    func prefix(s: string): prefixString = doPrefix
}
===
package main

type prefixString string

func doPrefix(s string) prefixString {
    return prefixString("[[" + string(s) + "]]")
}
===
print(main.prefix("This is a test"))
===
[[This is a test]]
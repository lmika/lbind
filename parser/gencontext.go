package parser

import "bitbucket.org/lmika/lbind/binds"

// Generator context information
type genContext struct {
	// The top-level bindings
	Bindings *binds.Bindings

	// The set of registered Go types
	Types map[string]binds.Type

	// The set of registered primitive script types
	PrimitiveScriptTypes map[string]binds.ScriptType
}

func newGenContext() *genContext {
	return &genContext{
		Types: map[string]binds.Type{
			"string":  binds.StringType,
			"int":     binds.IntType,
			"bool":    binds.BoolType,
			"float32": binds.Float32Type,
			"float64": binds.Float64Type,
			"error":   binds.ErrorType,
		},

		PrimitiveScriptTypes: map[string]binds.ScriptType{
			"string": binds.NativeScriptType("string"),
		},
	}
}

// Lookup a type based on it's name
func (gc *genContext) lookupType(typeName string) binds.Type {
	return gc.Types[typeName]
}

// Lookup an import
func (gc *genContext) lookupImport(name string) string {
	if pkg, hasPkg := gc.Bindings.Imports[name]; hasPkg {
		return pkg
	} else {
		return name
	}
}

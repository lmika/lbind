//go:generate goyacc -o parser.go parser.y
package parser

import (
	"fmt"
	"strings"

	"bitbucket.org/lmika/lbind/binds"
)

type astPackage struct {
	items *astPackageItems
}

func (ap *astPackage) BuildPackage(gc *genContext) (*binds.Bindings, error) {
	b := &binds.Bindings{
		Imports: make(map[string]string),
	}
	gc.Bindings = b

	if err := ap.items.Apply(gc, b); err != nil {
		return nil, err
	}

	return b, nil
}

type astPackageItems struct {
	head astPackageItem
	tail *astPackageItems
}

func (pi *astPackageItems) Apply(gc *genContext, b *binds.Bindings) error {
	if pi == nil {
		return nil
	}

	if err := pi.head.Apply(gc, b); err != nil {
		return err
	}

	return pi.tail.Apply(gc, b)
}

type astPackageItem interface {
	Apply(gc *genContext, b *binds.Bindings) error
}

type astPackageItemName struct {
	pos posSnapshot

	name string
}

func (pim *astPackageItemName) Apply(gc *genContext, b *binds.Bindings) error {
	b.Package = pim.name
	return nil
}

// An import
type astImportItem struct {
	alias   string
	pkgName string
}

func (im *astImportItem) Apply(gc *genContext, b *binds.Bindings) error {
	b.Imports[im.alias] = im.pkgName

	return nil
}

// A module declaration
type astModuleDecl struct {
	pos posSnapshot

	name  string
	items *astModuleItems
}

func (md *astModuleDecl) Apply(gc *genContext, b *binds.Bindings) error {
	module := &binds.Module{}

	module.Name = md.name
	module.GoName = md.name + "Module" // TODO: Parametertise
	module.Remarks = md.pos.lastComment

	if err := md.items.Apply(gc, module); err != nil {
		return err
	}

	b.Modules = append(b.Modules, module)
	return nil
}

// List of module items
type astModuleItems struct {
	head astModuleItem
	tail *astModuleItems
}

func (mi *astModuleItems) Apply(gc *genContext, m *binds.Module) error {
	if mi == nil {
		return nil
	}

	if err := mi.head.Apply(gc, m); err != nil {
		return err
	}

	return mi.tail.Apply(gc, m)
}

type astModuleItem interface {
	Apply(gc *genContext, m *binds.Module) error
}

type astClassTypeItem interface {
	astModuleItem

	ApplyToClassType(gc *genContext, ct *binds.ClassType) error
}

// Module variable item
type astModuleVar struct {
	pos posSnapshot

	name     string
	typeName astTypeExpr
}

func (mv *astModuleVar) Apply(gc *genContext, m *binds.Module) error {

	bindType, err := mv.typeName.ToType(gc)
	if err != nil {
		return fmt.Errorf("%s:%d: %v", mv.pos.pos.Filename, mv.pos.pos.Line, err)
	}

	m.Fields = append(m.Fields, &binds.ModuleField{
		Name: mv.name,
		Type: bindType,
	})
	return nil
}

// Function declaration module item
type astFuncDecl struct {
	pos posSnapshot

	name string
	args *astArgs
	rets *astArgs

	goFuncName astGoFnName
}

func (fd *astFuncDecl) Apply(gc *genContext, m *binds.Module) error {
	args, err := fd.args.MakeArgs(gc)
	if err != nil {
		return err
	}

	rets, err := fd.rets.MakeArgs(gc)
	if err != nil {
		return err
	}

	fmi := &binds.FunctionModuleItem{
		Name:       fd.name,
		GoFunction: fd.goFuncName.ToFnRes(gc),
		Args:       args,
		Rets:       rets,
		Remarks:    fd.pos.lastComment,
	}

	m.Functions = append(m.Functions, fmi)
	return nil
}

func (fd *astFuncDecl) ApplyToClassType(gc *genContext, ct *binds.ClassType) error {
	args, err := fd.args.MakeArgs(gc)
	if err != nil {
		return err
	}

	rets, err := fd.rets.MakeArgs(gc)
	if err != nil {
		return err
	}

	if len(args) > 0 {
		args[0].IsClassThis = true
	}

	fmi := &binds.FunctionModuleItem{
		Name:       fd.name,
		GoFunction: fd.goFuncName.ToFnRes(gc),
		Args:       args,
		Rets:       rets,
		Remarks:    fd.pos.lastComment,
	}

	ct.Methods = append(ct.Methods, fmi)
	return nil
}

type astArgs struct {
	head *astArg
	tail *astArgs
}

func (aa *astArgs) MakeArgs(gc *genContext) ([]*binds.FunctionArg, error) {
	fas := make([]*binds.FunctionArg, 0)
	for args := aa; args != nil; args = args.tail {
		fa, err := args.head.ToFunctionArg(gc)
		if err != nil {
			return nil, err
		}
		fas = append(fas, fa)
	}
	return fas, nil
}

type astArg struct {
	pos posSnapshot

	name     string
	typeName astTypeExpr
}

func (aa *astArg) ToFunctionArg(gc *genContext) (*binds.FunctionArg, error) {
	bindType, err := aa.typeName.ToType(gc)
	if err != nil {
		return nil, fmt.Errorf("%s:%d: %v", aa.pos.pos.Filename, aa.pos.pos.Line, err)
	}

	return &binds.FunctionArg{
		Name: aa.name,
		Type: bindType,
	}, nil
}

// Ast for Go function references
type astGoFnName interface {
	ToFnRes(gc *genContext) binds.GoFunctionRef
}

type astSingleGoFnName string

func (lf astSingleGoFnName) ToFnRes(gc *genContext) binds.GoFunctionRef {
	return binds.LocalGoFunctionRef(string(lf))
}

// A qualified Go function name.  This is a pair of identifiers separated by a "."
type astQualGoFnName struct {
	Qual string
	Name string
}

func (qf astQualGoFnName) ToFnRes(gc *genContext) binds.GoFunctionRef {
	if strings.HasPrefix(qf.Qual, "$") {
		varName := strings.TrimPrefix(qf.Qual, "$")
		return binds.ModuleVarFunctionRef{varName, qf.Name}
	} else {
		return binds.QualifiedGoFunctionRef{gc.lookupImport(qf.Qual), qf.Name}
	}
}

// astTypeExpr is the base type for type expressions
type astTypeExpr interface {
	// Returns the binding types
	ToType(gc *genContext) (binds.Type, error)
}

// A simple type name
type astSimpleType string

func (st astSimpleType) ToType(gc *genContext) (binds.Type, error) {
	bindType := gc.lookupType(string(st))
	if bindType != nil {
		return bindType, nil
	}
	return binds.LocalType(string(st)), nil
}

type astQualifiedType struct {
	Package string
	Name    string
}

func (qt astQualifiedType) ToType(gc *genContext) (binds.Type, error) {
	return binds.QualifiedType{
		Package: gc.lookupImport(qt.Package),
		Name:    qt.Name,
	}, nil
}

type astPointerType struct {
	derefType astTypeExpr
}

func (pt astPointerType) ToType(gc *genContext) (binds.Type, error) {
	dt, err := pt.derefType.ToType(gc)
	if err != nil {
		return nil, err
	}

	return binds.PointerType{dt}, nil
}

type astSliceType struct {
	sliceType astTypeExpr
}

func (pt astSliceType) ToType(gc *genContext) (binds.Type, error) {
	dt, err := pt.sliceType.ToType(gc)
	if err != nil {
		return nil, err
	}

	return binds.SliceType{dt}, nil
}

// Type mapping module item
type astTypeMapping struct {
	pos      posSnapshot
	typeName astTypeExpr
	target   astTypeMappingTarget
}

func (tm *astTypeMapping) Apply(gc *genContext, m *binds.Module) error {
	fromType, err := tm.typeName.ToType(gc)
	if err != nil {
		return fmt.Errorf("%s:%d: %v", tm.pos.pos.Filename, tm.pos.pos.Line, err)
	}

	toType, err := tm.target.ToScriptType(gc)
	if err != nil {
		return fmt.Errorf("%s:%d: %v", tm.pos.pos.Filename, tm.pos.pos.Line, err)
	}

	m.TypeMappings = append(m.TypeMappings, &binds.TypeMapping{
		GoType:     fromType,
		ScriptType: toType,
		Remarks:    tm.pos.lastComment,
	})
	return nil
}

type astTypeMappingTarget interface {
	ToScriptType(gc *genContext) (binds.ScriptType, error)
}

type astOpaqueTypeMappingTarget struct{}

func (otm astOpaqueTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	return binds.OpaqueScriptType{}, nil
}

type astArrayTypeMappingTarget struct{}

func (otm astArrayTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	return &binds.ArrayScriptType{}, nil
}

type astTableTypeMappingTarget struct {
	fieldMappings *astFieldMappings
}

func (ttm *astTableTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	fieldMappings := make([]*binds.FieldMapping, 0)

	for fms := ttm.fieldMappings; fms != nil; fms = fms.tail {
		fm, err := fms.head.ToFieldMapping(gc)
		if err != nil {
			return nil, err
		}
		fieldMappings = append(fieldMappings, fm)
	}

	return &binds.TableScriptType{
		FieldMappings: fieldMappings,
	}, nil
}

type astFieldMappings struct {
	head *astFieldMapping
	tail *astFieldMappings
}

type astFieldMapping struct {
	pos posSnapshot

	targetField string
	srcField    string
	srcType     astTypeExpr
}

func (fm *astFieldMapping) ToFieldMapping(gc *genContext) (*binds.FieldMapping, error) {
	goType, err := fm.srcType.ToType(gc)
	if err != nil {
		return nil, fmt.Errorf("%s:%d: field %s - %v", fm.pos.pos.Filename, fm.pos.pos.Line, fm.targetField, err)
	}

	return &binds.FieldMapping{
		ScriptField: fm.targetField,
		GoField:     fm.srcField,
		GoFieldType: goType,
	}, nil
}

type astClassTypeMappingTarget struct {
	name  string
	items *astModuleItems
}

func (ctm *astClassTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	classMapping := &binds.ClassType{
		Name: ctm.name,
	}

	for i := ctm.items; i != nil; i = i.tail {
		if classTypeItem, isClassTypeItem := i.head.(astClassTypeItem); isClassTypeItem {
			if err := classTypeItem.ApplyToClassType(gc, classMapping); err != nil {
				return nil, err
			}
		} else {
			return nil, fmt.Errorf("Item cannot be applied to class type: %v", i.head)
		}
	}

	return classMapping, nil
}

type astFnTypeMappingTarget struct {
	args *astArgs
	rets *astArgs
}

func (ftm *astFnTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	args, err := ftm.args.MakeArgs(gc)
	if err != nil {
		return nil, err
	}

	rets, err := ftm.rets.MakeArgs(gc)
	if err != nil {
		return nil, err
	}

	return &binds.FnScriptType{
		Args: args,
		Rets: rets,
	}, nil
}

type astNamedTypeMappingTarget struct {
	name string
}

func (ntm *astNamedTypeMappingTarget) ToScriptType(gc *genContext) (binds.ScriptType, error) {
	if primitiveScriptType, isPrimitiveScriptType := gc.PrimitiveScriptTypes[ntm.name]; isPrimitiveScriptType {
		return primitiveScriptType, nil
	}

	return nil, fmt.Errorf("Unrecognised type name: %s", ntm.name)
}

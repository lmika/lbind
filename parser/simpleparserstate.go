package parser

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"text/scanner"
	"unicode"
)

// Options for the scanner
type scannerOptions struct {
	// Mapping from runes produced by the scanner (including tokens)
	// and the parser tokens.
	TokenRunes map[rune]int

	// Functions used to set the lval for a particular scanner rune
	TokenLvals map[rune]scannerLvalFn

	// Identity tokens which are treated as keywords
	Keywords map[string]int
}

type scannerLvalFn func(lval *yySymType, s string) error

// A simple parser state incorporating the scanner which implements to go tools
// yacc lexer interface.
type simpleParserState struct {
	S                 scanner.Scanner
	options           *scannerOptions
	atEof             bool
	err               error
	lastCommentBuffer *bytes.Buffer
	lastCommentPos    scanner.Position
	parserResult      interface{}
}

func newSimpleParserState(src io.Reader, filename string, options *scannerOptions) *simpleParserState {
	ps := &simpleParserState{options: options}
	ps.lastCommentBuffer = new(bytes.Buffer)

	ps.S.Init(src)
	ps.S.Mode |= scanner.ScanComments
	ps.S.Mode &^= scanner.SkipComments

	ps.S.Position.Filename = filename

	return ps
}

func (ps *simpleParserState) Pos() scanner.Position {
	return ps.S.Pos()
}

func (ps *simpleParserState) Lex(lval *yySymType) int {
	if ps.atEof {
		return 0
	}
	for {
		tok := ps.S.Scan()

		// If the line of the next token is greater than the last comment line by 1,
		// clear the last comment.
		if ps.S.Pos().Line-ps.lastCommentPos.Line > 1 {
			ps.lastCommentBuffer.Reset()
		}

		switch tok {
		case scanner.EOF:
			ps.atEof = true
			return 0
		case scanner.Comment:
			ps.addComment(ps.S.TokenText())
			continue
		case scanner.Ident:
			if tokVal, isToken := ps.scanKeywordOrIdent(lval); isToken {
				return tokVal
			}
		default:
			if tokVal, isToken := ps.handleToken(lval, tok); isToken {
				return tokVal
			}
		}
	}
}

func (ps *simpleParserState) handleToken(lval *yySymType, tok rune) (int, bool) {
	if tokval, isToken := ps.options.TokenRunes[tok]; isToken {
		if fn, hasFn := ps.options.TokenLvals[tok]; hasFn {
			err := fn(lval, ps.S.TokenText())
			if err != nil {
				ps.Error("token " + scanner.TokenString(tok) + ": " + err.Error())
			}
		}
		return tokval, true
	} else {
		ps.Error("Invalid token: " + scanner.TokenString(tok))
		return -1, false
	}
}

func (ps *simpleParserState) scanKeywordOrIdent(lval *yySymType) (int, bool) {
	identStr := ps.S.TokenText()

	if tokVal, isToken := ps.options.Keywords[identStr]; isToken {
		return tokVal, true
	} else {
		return ps.handleToken(lval, scanner.Ident)
	}
}

// addComment adds a comment to the block.  The general rule for comment clustering
// is as follows:
//		TODO
func (ps *simpleParserState) addComment(c string) {
	// !!TEMP!!
	c = strings.TrimLeftFunc(strings.Replace(c, "//", "", 1), unicode.IsSpace)

	ps.lastCommentBuffer.WriteString(c + "\n")
	ps.lastCommentPos = ps.Pos()
}

// lastComment returns the last full comment block encountered.
func (ps *simpleParserState) lastComment() string {
	return ps.lastCommentBuffer.String()
}

func (ps *simpleParserState) Error(err string) {
	errMsg := fmt.Sprintf("%s:%d: %s", ps.S.Position.Filename, ps.S.Position.Line, err)
	ps.err = errors.New(errMsg)
}

type posSnapshot struct {
	pos         scanner.Position
	lastComment string
}

%{
package parser

import (
    "io"
    "strconv"
    "text/scanner"
)

var scannerOpts = &scannerOptions {
    TokenRunes: map[rune]int {
        '{': CURL,
        '}': CURR,
        '(': PARL,
        ')': PARR,
        '[': SQRL,
        ']': SQRR,
        '=': EQ,
        ',': COMMA,
        ':': COLON,
        '.': DOT,
        '*': ASTRISK,
        '$': DOLLAR,

        // scanner.Int: INT,
        scanner.Ident: IDENT,
        scanner.String: STRING,
    },

    TokenLvals: map[rune]scannerLvalFn {
        /*
        scanner.Int: func(lval *yySymType, s string) (err error) { 
            lval.ival, err = strconv.ParseInt(s, 10, 64)
            return
        },
        */

        scanner.Ident: func(lval *yySymType, s string) (err error) { 
            lval.sval = s
            return
        },

        scanner.String: func(lval *yySymType, s string) (err error) { 
            lval.sval, err = strconv.Unquote(s)
            return
        },
    },

    Keywords: map[string]int {
        "package": K_PACKAGE,
        "import": K_IMPORT,
        "module": K_MODULE,
        "var": K_VAR,
        "func": K_FUNC,
        "typemap": K_TYPEMAP,
        "opaque": K_OPAQUE,
        "array": K_ARRAY,
        "table": K_TABLE,
        "class": K_CLASS,
    },
}

var lastScannedComment string
%}

%union {
    sval                string
    pos                 posSnapshot

    pack                *astPackage
    packItems           *astPackageItems
    packItem            astPackageItem
    moduleItems         *astModuleItems
    moduleItem          astModuleItem
    goFnName            astGoFnName
    typeExpr            astTypeExpr
    args                *astArgs
    arg                 *astArg
    typeMapping         astTypeMapping
    typeMappingTarget   astTypeMappingTarget
    fieldMappings       *astFieldMappings
    fieldMapping        *astFieldMapping
}

%token  EQ  COMMA COLON DOT ASTRISK DOLLAR
%token  CURL CURR PARL PARR SQRL SQRR
%token  <sval> IDENT STRING

%token  K_PACKAGE  K_MODULE  K_IMPORT
%token  K_VAR K_FUNC K_ARRAY K_TABLE K_CLASS K_FUNC
%token  K_TYPEMAP K_OPAQUE

%type   <packItems>     packageitems
%type   <packItem>      packageitem packagename importitem moduledecl
%type   <moduleItems>   moduleitems
%type   <moduleItem>    moduleitem funcdecl modulefield typemapping
%type   <typeMappingTarget> typemappingtarget
%type   <args>          funcpars maybearglist arglist funcrets
%type   <arg>           arg
%type   <pos>           pos
%type   <goFnName>      gofnname
%type   <sval>          identorstring
%type   <typeExpr>      typeexpr
%type   <fieldMappings> tablefieldmappings
%type   <fieldMapping>  tablefieldmapping

%%

top
    : packageitems {
        yylex.(*simpleParserState).parserResult = &astPackage{$1};
    }
    ;

packageitems
    : /* empty */ {
        $$ = nil
    }
    | packageitem packageitems {
        $$ = &astPackageItems{$1, $2}
    }
    ;

packageitem
    : packagename {
        $$ = $1
    }
    | importitem {
        $$ = $1
    }
    | moduledecl {
        $$ = $1
    }
    ;

packagename
    : pos K_PACKAGE IDENT {
        $$ = &astPackageItemName{$1, $3}
    }
    ;

importitem
    : K_IMPORT IDENT STRING {
        $$ = &astImportItem{$2, $3}
    }
    ;

moduledecl
    : pos K_MODULE IDENT CURL moduleitems CURR {
        $$ = &astModuleDecl{$1, $3, $5}
    }
    ;

moduleitems
    : /* empty */ {
        $$ = nil
    }
    | moduleitem moduleitems {
        $$ = &astModuleItems{$1, $2}
    }
    ;

moduleitem
    : funcdecl {
        $$ = $1
    }
    | modulefield {
        $$ = $1;
    }
    | typemapping {
        $$ = $1;
    }
    ;

modulefield
    : pos K_VAR IDENT COLON typeexpr {
        $$ = &astModuleVar{$1, $3, $5}
    }
    ;

funcdecl
    : pos K_FUNC identorstring funcpars funcrets EQ gofnname {
        $$ = &astFuncDecl{$1, $3, $4, $5, $7}
    }
    ;

gofnname
    : IDENT {
        $$ = astSingleGoFnName($1)
    }
    | identorstring DOT IDENT {
        $$ = astQualGoFnName{$1, $3}
    }
    | DOLLAR IDENT DOT IDENT {
        $$ = astQualGoFnName{"$" + $2, $4}
    }
    ;

funcpars
    : PARL maybearglist PARR {
        $$ = $2
    }
    ;

maybearglist
    : /* empty */ {
        $$ = nil
    }
    | arglist {
        $$ = $1
    }
    ;

arglist
    : arg {
        $$ = &astArgs{$1, nil}
    }
    | arg COMMA arglist {
        $$ = &astArgs{$1, $3}
    }
    ;

arg
    : pos IDENT COLON typeexpr {
        $$ = &astArg{$1, $2, $4}
    }
    | pos typeexpr {
        $$ = &astArg{$1, "", $2}
    }
    ;

funcrets
    : /* empty */ {
        $$ = nil
    }
    | COLON typeexpr {
        pos := yylex.(*simpleParserState).Pos()     // Not exactly correct, but prevents a shift/reduce conflict
        $$ = &astArgs{&astArg{posSnapshot{pos, ""}, "", $2}, nil}
    }
    | COLON PARL arglist PARR {
        $$ = $3
    }
    ;

typeexpr
    : IDENT {
        $$ = astSimpleType($1)
    }
    | identorstring DOT IDENT {
        $$ = astQualifiedType{$1, $3}
    }
    | ASTRISK typeexpr {
        $$ = astPointerType{$2}
    }
    | SQRL SQRR typeexpr {
        $$ = astSliceType{$3}
    }
    ;

typemapping
    : pos K_TYPEMAP typeexpr EQ typemappingtarget {
        $$ = &astTypeMapping{$1, $3, $5}
    }
    ;

typemappingtarget
    : K_OPAQUE {
        $$ = astOpaqueTypeMappingTarget{}
    }
    | K_ARRAY {
        $$ = astArrayTypeMappingTarget{}
    }
    | K_TABLE CURL tablefieldmappings CURR {
        $$ = &astTableTypeMappingTarget{$3}
    }
    | K_CLASS IDENT CURL moduleitems CURR {
        $$ = &astClassTypeMappingTarget{$2, $4}
    }
    | K_FUNC funcpars funcrets {
        $$ = &astFnTypeMappingTarget{$2, $3}
    }
    | IDENT {
        $$ = &astNamedTypeMappingTarget{$1}
    }
    ;

tablefieldmappings
    : /* empty */ {
        $$ = nil
    }
    | tablefieldmapping tablefieldmappings {
        $$ = &astFieldMappings{$1, $2}
    }
    ;

tablefieldmapping
    : pos IDENT EQ IDENT COLON typeexpr {
        $$ = &astFieldMapping{$1, $2, $4, $6}
    }
    ;

identorstring
    : IDENT {
        $$ = $1
    }
    | STRING {
        $$ = $1
    }
    ;

pos
    : /* position snapshot */ {
        pos := yylex.(*simpleParserState).Pos()
        lastScannedComment := yylex.(*simpleParserState).lastComment()

        $$ = posSnapshot{pos, lastScannedComment}
    }
    ;

%%

func parse(reader io.Reader, filename string) (*astPackage, error) {
    ps := newSimpleParserState(reader, filename, scannerOpts)
    yyParse(ps)

    if ps.err != nil {
        return nil, ps.err
    } else {
        return ps.parserResult.(*astPackage), nil
    }
}
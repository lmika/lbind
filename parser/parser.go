//line parser.y:2
package parser

import __yyfmt__ "fmt"

//line parser.y:2
import (
	"io"
	"strconv"
	"text/scanner"
)

var scannerOpts = &scannerOptions{
	TokenRunes: map[rune]int{
		'{': CURL,
		'}': CURR,
		'(': PARL,
		')': PARR,
		'[': SQRL,
		']': SQRR,
		'=': EQ,
		',': COMMA,
		':': COLON,
		'.': DOT,
		'*': ASTRISK,
		'$': DOLLAR,

		// scanner.Int: INT,
		scanner.Ident:  IDENT,
		scanner.String: STRING,
	},

	TokenLvals: map[rune]scannerLvalFn{
		/*
		   scanner.Int: func(lval *yySymType, s string) (err error) {
		       lval.ival, err = strconv.ParseInt(s, 10, 64)
		       return
		   },
		*/

		scanner.Ident: func(lval *yySymType, s string) (err error) {
			lval.sval = s
			return
		},

		scanner.String: func(lval *yySymType, s string) (err error) {
			lval.sval, err = strconv.Unquote(s)
			return
		},
	},

	Keywords: map[string]int{
		"package": K_PACKAGE,
		"import":  K_IMPORT,
		"module":  K_MODULE,
		"var":     K_VAR,
		"func":    K_FUNC,
		"typemap": K_TYPEMAP,
		"opaque":  K_OPAQUE,
		"array":   K_ARRAY,
		"table":   K_TABLE,
		"class":   K_CLASS,
	},
}

var lastScannedComment string

//line parser.y:66
type yySymType struct {
	yys  int
	sval string
	pos  posSnapshot

	pack              *astPackage
	packItems         *astPackageItems
	packItem          astPackageItem
	moduleItems       *astModuleItems
	moduleItem        astModuleItem
	goFnName          astGoFnName
	typeExpr          astTypeExpr
	args              *astArgs
	arg               *astArg
	typeMapping       astTypeMapping
	typeMappingTarget astTypeMappingTarget
	fieldMappings     *astFieldMappings
	fieldMapping      *astFieldMapping
}

const EQ = 57346
const COMMA = 57347
const COLON = 57348
const DOT = 57349
const ASTRISK = 57350
const DOLLAR = 57351
const CURL = 57352
const CURR = 57353
const PARL = 57354
const PARR = 57355
const SQRL = 57356
const SQRR = 57357
const IDENT = 57358
const STRING = 57359
const K_PACKAGE = 57360
const K_MODULE = 57361
const K_IMPORT = 57362
const K_VAR = 57363
const K_FUNC = 57364
const K_ARRAY = 57365
const K_TABLE = 57366
const K_CLASS = 57367
const K_TYPEMAP = 57368
const K_OPAQUE = 57369

var yyToknames = [...]string{
	"$end",
	"error",
	"$unk",
	"EQ",
	"COMMA",
	"COLON",
	"DOT",
	"ASTRISK",
	"DOLLAR",
	"CURL",
	"CURR",
	"PARL",
	"PARR",
	"SQRL",
	"SQRR",
	"IDENT",
	"STRING",
	"K_PACKAGE",
	"K_MODULE",
	"K_IMPORT",
	"K_VAR",
	"K_FUNC",
	"K_ARRAY",
	"K_TABLE",
	"K_CLASS",
	"K_TYPEMAP",
	"K_OPAQUE",
}
var yyStatenames = [...]string{}

const yyEofCode = 1
const yyErrCode = 2
const yyInitialStackSize = 16

//line parser.y:320

func parse(reader io.Reader, filename string) (*astPackage, error) {
	ps := newSimpleParserState(reader, filename, scannerOpts)
	yyParse(ps)

	if ps.err != nil {
		return nil, ps.err
	} else {
		return ps.parserResult.(*astPackage), nil
	}
}

//line yacctab:1
var yyExca = [...]int{
	-1, 0,
	1, 2,
	-2, 46,
	-1, 1,
	1, -1,
	-2, 0,
	-1, 3,
	1, 2,
	-2, 46,
	-1, 16,
	11, 10,
	-2, 46,
	-1, 18,
	11, 10,
	-2, 46,
	-1, 33,
	7, 44,
	-2, 30,
	-1, 39,
	13, 21,
	-2, 46,
	-1, 65,
	7, 44,
	-2, 30,
	-1, 67,
	16, 46,
	-2, 41,
	-1, 71,
	7, 44,
	-2, 17,
	-1, 78,
	16, 46,
	-2, 41,
	-1, 80,
	11, 10,
	-2, 46,
}

const yyPrivate = 57344

const yyLast = 110

var yyAct = [...]int{

	32, 22, 7, 17, 77, 7, 45, 48, 38, 57,
	34, 8, 10, 11, 15, 56, 53, 54, 55, 95,
	52, 35, 24, 25, 26, 62, 35, 36, 27, 33,
	31, 94, 36, 90, 33, 31, 42, 29, 44, 88,
	35, 50, 30, 31, 59, 73, 36, 61, 65, 31,
	83, 66, 71, 31, 68, 58, 28, 14, 13, 12,
	43, 84, 63, 39, 50, 69, 50, 93, 86, 79,
	74, 72, 75, 23, 80, 67, 81, 85, 16, 91,
	79, 82, 41, 87, 89, 96, 46, 76, 37, 64,
	92, 60, 40, 1, 2, 78, 70, 97, 9, 49,
	47, 51, 21, 20, 19, 18, 6, 5, 4, 3,
}
var yyPact = [...]int{

	-9, -1000, -1000, -9, -1000, -1000, -1000, -6, 43, -1000,
	42, 41, -3, -1000, 68, -1000, -1000, 62, -1000, -1000,
	-1000, -1000, 2, -1000, -1000, 40, 26, 18, 82, 51,
	-1000, -1000, 88, -1000, 75, 18, 45, 18, 80, -1000,
	-7, 39, -1000, 18, -1000, 87, 13, 49, -1000, 84,
	32, -1000, -1000, -1000, 65, 38, 51, -1000, -1000, -1000,
	36, -1000, -1000, -1000, -1000, 81, -1000, -1000, 64, 80,
	-1000, -1000, 74, 34, 48, -1000, 18, 57, -1000, 23,
	-1000, -1000, 17, 72, -1000, -1000, -1000, -1000, 86, 56,
	-1000, 15, 3, -1000, -1000, 79, 18, -1000,
}
var yyPgo = [...]int{

	0, 94, 109, 108, 107, 106, 3, 105, 104, 103,
	102, 101, 8, 100, 7, 6, 99, 1, 96, 10,
	0, 4, 95, 93,
}
var yyR1 = [...]int{

	0, 23, 1, 1, 2, 2, 2, 3, 4, 5,
	6, 6, 7, 7, 7, 9, 8, 18, 18, 18,
	12, 13, 13, 14, 14, 16, 16, 15, 15, 15,
	20, 20, 20, 20, 10, 11, 11, 11, 11, 11,
	11, 21, 21, 22, 19, 19, 17,
}
var yyR2 = [...]int{

	0, 1, 0, 2, 1, 1, 1, 3, 3, 6,
	0, 2, 1, 1, 1, 5, 7, 1, 3, 4,
	3, 0, 1, 1, 3, 4, 2, 0, 2, 4,
	1, 3, 2, 3, 5, 1, 1, 4, 5, 3,
	1, 0, 2, 6, 1, 1, 0,
}
var yyChk = [...]int{

	-1000, -23, -1, -2, -3, -4, -5, -17, 20, -1,
	18, 19, 16, 16, 16, 17, 10, -6, -7, -8,
	-9, -10, -17, 11, -6, 21, 22, 26, 16, -19,
	16, 17, -20, 16, -19, 8, 14, 6, -12, 12,
	4, 7, -20, 15, -20, -15, 6, -13, -14, -16,
	-17, -11, 27, 23, 24, 25, 22, 16, 16, -20,
	4, -20, 12, 13, 5, 16, -20, 10, 16, -12,
	-18, 16, -19, 9, -14, -14, 6, -21, -22, -17,
	10, -15, 7, 16, 13, -20, 11, -21, 16, -6,
	16, 7, 4, 11, 16, 16, 6, -20,
}
var yyDef = [...]int{

	-2, -2, 1, -2, 4, 5, 6, 0, 0, 3,
	0, 0, 0, 7, 0, 8, -2, 0, -2, 12,
	13, 14, 0, 9, 11, 0, 0, 0, 0, 0,
	44, 45, 0, -2, 0, 0, 0, 0, 27, -2,
	0, 0, 32, 0, 15, 0, 0, 0, 22, 23,
	0, 34, 35, 36, 0, 0, 0, 40, 31, 33,
	0, 28, 46, 20, 46, -2, 26, -2, 0, 27,
	16, -2, 0, 0, 0, 24, 0, 0, -2, 0,
	-2, 39, 0, 0, 29, 25, 37, 42, 0, 0,
	18, 0, 0, 38, 19, 0, 0, 43,
}
var yyTok1 = [...]int{

	1,
}
var yyTok2 = [...]int{

	2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
	12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
	22, 23, 24, 25, 26, 27,
}
var yyTok3 = [...]int{
	0,
}

var yyErrorMessages = [...]struct {
	state int
	token int
	msg   string
}{}

//line yaccpar:1

/*	parser for yacc output	*/

var (
	yyDebug        = 0
	yyErrorVerbose = false
)

type yyLexer interface {
	Lex(lval *yySymType) int
	Error(s string)
}

type yyParser interface {
	Parse(yyLexer) int
	Lookahead() int
}

type yyParserImpl struct {
	lval  yySymType
	stack [yyInitialStackSize]yySymType
	char  int
}

func (p *yyParserImpl) Lookahead() int {
	return p.char
}

func yyNewParser() yyParser {
	return &yyParserImpl{}
}

const yyFlag = -1000

func yyTokname(c int) string {
	if c >= 1 && c-1 < len(yyToknames) {
		if yyToknames[c-1] != "" {
			return yyToknames[c-1]
		}
	}
	return __yyfmt__.Sprintf("tok-%v", c)
}

func yyStatname(s int) string {
	if s >= 0 && s < len(yyStatenames) {
		if yyStatenames[s] != "" {
			return yyStatenames[s]
		}
	}
	return __yyfmt__.Sprintf("state-%v", s)
}

func yyErrorMessage(state, lookAhead int) string {
	const TOKSTART = 4

	if !yyErrorVerbose {
		return "syntax error"
	}

	for _, e := range yyErrorMessages {
		if e.state == state && e.token == lookAhead {
			return "syntax error: " + e.msg
		}
	}

	res := "syntax error: unexpected " + yyTokname(lookAhead)

	// To match Bison, suggest at most four expected tokens.
	expected := make([]int, 0, 4)

	// Look for shiftable tokens.
	base := yyPact[state]
	for tok := TOKSTART; tok-1 < len(yyToknames); tok++ {
		if n := base + tok; n >= 0 && n < yyLast && yyChk[yyAct[n]] == tok {
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}
	}

	if yyDef[state] == -2 {
		i := 0
		for yyExca[i] != -1 || yyExca[i+1] != state {
			i += 2
		}

		// Look for tokens that we accept or reduce.
		for i += 2; yyExca[i] >= 0; i += 2 {
			tok := yyExca[i]
			if tok < TOKSTART || yyExca[i+1] == 0 {
				continue
			}
			if len(expected) == cap(expected) {
				return res
			}
			expected = append(expected, tok)
		}

		// If the default action is to accept or reduce, give up.
		if yyExca[i+1] != 0 {
			return res
		}
	}

	for i, tok := range expected {
		if i == 0 {
			res += ", expecting "
		} else {
			res += " or "
		}
		res += yyTokname(tok)
	}
	return res
}

func yylex1(lex yyLexer, lval *yySymType) (char, token int) {
	token = 0
	char = lex.Lex(lval)
	if char <= 0 {
		token = yyTok1[0]
		goto out
	}
	if char < len(yyTok1) {
		token = yyTok1[char]
		goto out
	}
	if char >= yyPrivate {
		if char < yyPrivate+len(yyTok2) {
			token = yyTok2[char-yyPrivate]
			goto out
		}
	}
	for i := 0; i < len(yyTok3); i += 2 {
		token = yyTok3[i+0]
		if token == char {
			token = yyTok3[i+1]
			goto out
		}
	}

out:
	if token == 0 {
		token = yyTok2[1] /* unknown char */
	}
	if yyDebug >= 3 {
		__yyfmt__.Printf("lex %s(%d)\n", yyTokname(token), uint(char))
	}
	return char, token
}

func yyParse(yylex yyLexer) int {
	return yyNewParser().Parse(yylex)
}

func (yyrcvr *yyParserImpl) Parse(yylex yyLexer) int {
	var yyn int
	var yyVAL yySymType
	var yyDollar []yySymType
	_ = yyDollar // silence set and not used
	yyS := yyrcvr.stack[:]

	Nerrs := 0   /* number of errors */
	Errflag := 0 /* error recovery flag */
	yystate := 0
	yyrcvr.char = -1
	yytoken := -1 // yyrcvr.char translated into internal numbering
	defer func() {
		// Make sure we report no lookahead when not parsing.
		yystate = -1
		yyrcvr.char = -1
		yytoken = -1
	}()
	yyp := -1
	goto yystack

ret0:
	return 0

ret1:
	return 1

yystack:
	/* put a state and value onto the stack */
	if yyDebug >= 4 {
		__yyfmt__.Printf("char %v in %v\n", yyTokname(yytoken), yyStatname(yystate))
	}

	yyp++
	if yyp >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyS[yyp] = yyVAL
	yyS[yyp].yys = yystate

yynewstate:
	yyn = yyPact[yystate]
	if yyn <= yyFlag {
		goto yydefault /* simple state */
	}
	if yyrcvr.char < 0 {
		yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
	}
	yyn += yytoken
	if yyn < 0 || yyn >= yyLast {
		goto yydefault
	}
	yyn = yyAct[yyn]
	if yyChk[yyn] == yytoken { /* valid shift */
		yyrcvr.char = -1
		yytoken = -1
		yyVAL = yyrcvr.lval
		yystate = yyn
		if Errflag > 0 {
			Errflag--
		}
		goto yystack
	}

yydefault:
	/* default state action */
	yyn = yyDef[yystate]
	if yyn == -2 {
		if yyrcvr.char < 0 {
			yyrcvr.char, yytoken = yylex1(yylex, &yyrcvr.lval)
		}

		/* look through exception table */
		xi := 0
		for {
			if yyExca[xi+0] == -1 && yyExca[xi+1] == yystate {
				break
			}
			xi += 2
		}
		for xi += 2; ; xi += 2 {
			yyn = yyExca[xi+0]
			if yyn < 0 || yyn == yytoken {
				break
			}
		}
		yyn = yyExca[xi+1]
		if yyn < 0 {
			goto ret0
		}
	}
	if yyn == 0 {
		/* error ... attempt to resume parsing */
		switch Errflag {
		case 0: /* brand new error */
			yylex.Error(yyErrorMessage(yystate, yytoken))
			Nerrs++
			if yyDebug >= 1 {
				__yyfmt__.Printf("%s", yyStatname(yystate))
				__yyfmt__.Printf(" saw %s\n", yyTokname(yytoken))
			}
			fallthrough

		case 1, 2: /* incompletely recovered error ... try again */
			Errflag = 3

			/* find a state where "error" is a legal shift action */
			for yyp >= 0 {
				yyn = yyPact[yyS[yyp].yys] + yyErrCode
				if yyn >= 0 && yyn < yyLast {
					yystate = yyAct[yyn] /* simulate a shift of "error" */
					if yyChk[yystate] == yyErrCode {
						goto yystack
					}
				}

				/* the current p has no shift on "error", pop stack */
				if yyDebug >= 2 {
					__yyfmt__.Printf("error recovery pops state %d\n", yyS[yyp].yys)
				}
				yyp--
			}
			/* there is no state on the stack with an error shift ... abort */
			goto ret1

		case 3: /* no shift yet; clobber input char */
			if yyDebug >= 2 {
				__yyfmt__.Printf("error recovery discards %s\n", yyTokname(yytoken))
			}
			if yytoken == yyEofCode {
				goto ret1
			}
			yyrcvr.char = -1
			yytoken = -1
			goto yynewstate /* try again in the same state */
		}
	}

	/* reduction by production yyn */
	if yyDebug >= 2 {
		__yyfmt__.Printf("reduce %v in:\n\t%v\n", yyn, yyStatname(yystate))
	}

	yynt := yyn
	yypt := yyp
	_ = yypt // guard against "declared and not used"

	yyp -= yyR2[yyn]
	// yyp is now the index of $0. Perform the default action. Iff the
	// reduced production is ε, $1 is possibly out of range.
	if yyp+1 >= len(yyS) {
		nyys := make([]yySymType, len(yyS)*2)
		copy(nyys, yyS)
		yyS = nyys
	}
	yyVAL = yyS[yyp+1]

	/* consult goto table to find next state */
	yyn = yyR1[yyn]
	yyg := yyPgo[yyn]
	yyj := yyg + yyS[yyp].yys + 1

	if yyj >= yyLast {
		yystate = yyAct[yyg]
	} else {
		yystate = yyAct[yyj]
		if yyChk[yystate] != -yyn {
			yystate = yyAct[yyg]
		}
	}
	// dummy call; replaced with literal code
	switch yynt {

	case 1:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:110
		{
			yylex.(*simpleParserState).parserResult = &astPackage{yyDollar[1].packItems}
		}
	case 2:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:116
		{
			yyVAL.packItems = nil
		}
	case 3:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:119
		{
			yyVAL.packItems = &astPackageItems{yyDollar[1].packItem, yyDollar[2].packItems}
		}
	case 4:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:125
		{
			yyVAL.packItem = yyDollar[1].packItem
		}
	case 5:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:128
		{
			yyVAL.packItem = yyDollar[1].packItem
		}
	case 6:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:131
		{
			yyVAL.packItem = yyDollar[1].packItem
		}
	case 7:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:137
		{
			yyVAL.packItem = &astPackageItemName{yyDollar[1].pos, yyDollar[3].sval}
		}
	case 8:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:143
		{
			yyVAL.packItem = &astImportItem{yyDollar[2].sval, yyDollar[3].sval}
		}
	case 9:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line parser.y:149
		{
			yyVAL.packItem = &astModuleDecl{yyDollar[1].pos, yyDollar[3].sval, yyDollar[5].moduleItems}
		}
	case 10:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:155
		{
			yyVAL.moduleItems = nil
		}
	case 11:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:158
		{
			yyVAL.moduleItems = &astModuleItems{yyDollar[1].moduleItem, yyDollar[2].moduleItems}
		}
	case 12:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:164
		{
			yyVAL.moduleItem = yyDollar[1].moduleItem
		}
	case 13:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:167
		{
			yyVAL.moduleItem = yyDollar[1].moduleItem
		}
	case 14:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:170
		{
			yyVAL.moduleItem = yyDollar[1].moduleItem
		}
	case 15:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line parser.y:176
		{
			yyVAL.moduleItem = &astModuleVar{yyDollar[1].pos, yyDollar[3].sval, yyDollar[5].typeExpr}
		}
	case 16:
		yyDollar = yyS[yypt-7 : yypt+1]
		//line parser.y:182
		{
			yyVAL.moduleItem = &astFuncDecl{yyDollar[1].pos, yyDollar[3].sval, yyDollar[4].args, yyDollar[5].args, yyDollar[7].goFnName}
		}
	case 17:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:188
		{
			yyVAL.goFnName = astSingleGoFnName(yyDollar[1].sval)
		}
	case 18:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:191
		{
			yyVAL.goFnName = astQualGoFnName{yyDollar[1].sval, yyDollar[3].sval}
		}
	case 19:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:194
		{
			yyVAL.goFnName = astQualGoFnName{"$" + yyDollar[2].sval, yyDollar[4].sval}
		}
	case 20:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:200
		{
			yyVAL.args = yyDollar[2].args
		}
	case 21:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:206
		{
			yyVAL.args = nil
		}
	case 22:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:209
		{
			yyVAL.args = yyDollar[1].args
		}
	case 23:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:215
		{
			yyVAL.args = &astArgs{yyDollar[1].arg, nil}
		}
	case 24:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:218
		{
			yyVAL.args = &astArgs{yyDollar[1].arg, yyDollar[3].args}
		}
	case 25:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:224
		{
			yyVAL.arg = &astArg{yyDollar[1].pos, yyDollar[2].sval, yyDollar[4].typeExpr}
		}
	case 26:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:227
		{
			yyVAL.arg = &astArg{yyDollar[1].pos, "", yyDollar[2].typeExpr}
		}
	case 27:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:233
		{
			yyVAL.args = nil
		}
	case 28:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:236
		{
			pos := yylex.(*simpleParserState).Pos() // Not exactly correct, but prevents a shift/reduce conflict
			yyVAL.args = &astArgs{&astArg{posSnapshot{pos, ""}, "", yyDollar[2].typeExpr}, nil}
		}
	case 29:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:240
		{
			yyVAL.args = yyDollar[3].args
		}
	case 30:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:246
		{
			yyVAL.typeExpr = astSimpleType(yyDollar[1].sval)
		}
	case 31:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:249
		{
			yyVAL.typeExpr = astQualifiedType{yyDollar[1].sval, yyDollar[3].sval}
		}
	case 32:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:252
		{
			yyVAL.typeExpr = astPointerType{yyDollar[2].typeExpr}
		}
	case 33:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:255
		{
			yyVAL.typeExpr = astSliceType{yyDollar[3].typeExpr}
		}
	case 34:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line parser.y:261
		{
			yyVAL.moduleItem = &astTypeMapping{yyDollar[1].pos, yyDollar[3].typeExpr, yyDollar[5].typeMappingTarget}
		}
	case 35:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:267
		{
			yyVAL.typeMappingTarget = astOpaqueTypeMappingTarget{}
		}
	case 36:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:270
		{
			yyVAL.typeMappingTarget = astArrayTypeMappingTarget{}
		}
	case 37:
		yyDollar = yyS[yypt-4 : yypt+1]
		//line parser.y:273
		{
			yyVAL.typeMappingTarget = &astTableTypeMappingTarget{yyDollar[3].fieldMappings}
		}
	case 38:
		yyDollar = yyS[yypt-5 : yypt+1]
		//line parser.y:276
		{
			yyVAL.typeMappingTarget = &astClassTypeMappingTarget{yyDollar[2].sval, yyDollar[4].moduleItems}
		}
	case 39:
		yyDollar = yyS[yypt-3 : yypt+1]
		//line parser.y:279
		{
			yyVAL.typeMappingTarget = &astFnTypeMappingTarget{yyDollar[2].args, yyDollar[3].args}
		}
	case 40:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:282
		{
			yyVAL.typeMappingTarget = &astNamedTypeMappingTarget{yyDollar[1].sval}
		}
	case 41:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:288
		{
			yyVAL.fieldMappings = nil
		}
	case 42:
		yyDollar = yyS[yypt-2 : yypt+1]
		//line parser.y:291
		{
			yyVAL.fieldMappings = &astFieldMappings{yyDollar[1].fieldMapping, yyDollar[2].fieldMappings}
		}
	case 43:
		yyDollar = yyS[yypt-6 : yypt+1]
		//line parser.y:297
		{
			yyVAL.fieldMapping = &astFieldMapping{yyDollar[1].pos, yyDollar[2].sval, yyDollar[4].sval, yyDollar[6].typeExpr}
		}
	case 44:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:303
		{
			yyVAL.sval = yyDollar[1].sval
		}
	case 45:
		yyDollar = yyS[yypt-1 : yypt+1]
		//line parser.y:306
		{
			yyVAL.sval = yyDollar[1].sval
		}
	case 46:
		yyDollar = yyS[yypt-0 : yypt+1]
		//line parser.y:312
		{
			pos := yylex.(*simpleParserState).Pos()
			lastScannedComment := yylex.(*simpleParserState).lastComment()

			yyVAL.pos = posSnapshot{pos, lastScannedComment}
		}
	}
	goto yystack /* stack new state and value */
}

package parser

import (
	"io"

	"bitbucket.org/lmika/lbind/binds"
)

// Parse parses a binding declaration
func Parse(r io.Reader) (*binds.Bindings, error) {
	astPack, err := parse(r, "stdin")
	if err != nil {
		return nil, err
	}

	gc := newGenContext()

	return astPack.BuildPackage(gc)
}

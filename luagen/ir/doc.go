// IR represents the intermediate language used to generate the lua bindings.
// The purpose of this is that there are three different Lua VM implementataions
// with slightly different APIs and it would be nice for lbind to support all
// three.  Hence the purpose of this package is to define the abstract
// stack operations with the targets generating the appropriate code to implement
// them.
package ir

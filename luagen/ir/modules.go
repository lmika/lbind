package ir

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
)

// Module represents a generated module.
type Module struct {
	Src *binds.Module

	BindFns []*BindFn
	RegFn   *BindFn
}

// addFunctions add all bind functions and registration functions to the given
// module.
func (m *Module) addFunctions(fns []*binds.FunctionModuleItem) error {
	// Add all the bind functions
	for _, fn := range fns {
		if err := m.addBindFunction(fn); err != nil {
			return err
		}
	}

	return nil
}

// addFunction generates and adds the bind function for the passed in function
func (m *Module) addBindFunction(fn *binds.FunctionModuleItem) error {
	bindFn, err := m.genBindFunction(fn)
	if err != nil {
		return err
	}

	m.BindFns = append(m.BindFns, bindFn)
	return nil
}

func (m *Module) genBindFunction(fn *binds.FunctionModuleItem) (*BindFn, error) {
	bf := &BindFn{}

	var argNames, retNames []string

	// Work out arguments and return values
	for _, arg := range fn.Args {
		argNames = append(argNames, arg.Name)
	}

	for i, ret := range fn.Rets {
		name := ret.Name
		if name == "" {
			name = fmt.Sprintf("ret%d", i)
		}
		retNames = append(retNames, name)
	}

	// Unmarshall arguments
	if err := bf.unmarshalArgs(fn.Args, 1); err != nil {
		return nil, err
	}

	// Call the mapped function
	if err := bf.callGoFunctionRef(fn.GoFunction, argNames, retNames); err != nil {
		return nil, err
	}

	// Marshal the results
	retCount, err := bf.marshalResults(fn.Rets)
	if err != nil {
		return nil, err
	}

	bf.op(ReturnIntOp{IntVal: retCount})

	return bf, nil
}

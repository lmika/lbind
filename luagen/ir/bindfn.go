package ir

import (
	"fmt"

	"bitbucket.org/lmika/lbind/binds"
)

// BindFn represents a bind function.  Each bind function takes a goState as an
// argument and is expected to return an integer representing the number of
// items left on the stack.
type BindFn struct {
	Ops []Op
}

// unmarshalArgs adds the code to unmarshal arguments from the opstack
func (bf *BindFn) unmarshalArgs(args []*binds.FunctionArg, offset int) error {
	for i, arg := range args {
		if err := bf.unmarshalArg(arg, offset+i); err != nil {
			return err
		}
	}
	return nil
}

// unmarshalArg adds the code to unmarshal the given argument.  This
// reads the argument values from the opstack into Go variables.
func (bf *BindFn) unmarshalArg(arg *binds.FunctionArg, pos int) error {
	// TODO: Do proper type mapping, etc.
	bf.op(ToStackOp{ToVar: arg.Name, Pos: pos})
	return nil
}

// callGoFunctionRef adds the code to call the go function reference
func (bf *BindFn) callGoFunctionRef(ref binds.GoFunctionRef, argNames []string, retNames []string) error {

	switch r := ref.(type) {
	case binds.LocalGoFunctionRef:
		bf.op(CallNativeFunctionOp{Name: string(r), ArgVars: argNames, RetVars: retNames})
		return nil
	}

	return fmt.Errorf("Unable to handle go function ref: %#v", ref)
}

// marshalResults marshal the results from the go function
func (bf *BindFn) marshalResults(rets []*binds.FunctionArg) (resultsPushed int, err error) {
	// TODO: Handle throwing of errors

	for _, ret := range rets {
		if err := bf.marshalResult(ret); err != nil {
			return 0, err
		}
	}

	return len(rets), nil
}

// marshalResult marshal a results from a go function
func (bf *BindFn) marshalResult(ret *binds.FunctionArg) error {
	// TODO: Do proper type mapping
	bf.op(PushStackOp{FromVar: "ret0"})
	return nil
}

// op adds an operation
func (bf *BindFn) op(op Op) {
	bf.Ops = append(bf.Ops, op)
}

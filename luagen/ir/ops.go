package ir

import "bitbucket.org/lmika/lbind/binds"

// Op represents an abstract operation that will appear within a bind function.
type Op interface {
}

// Block contains a collection of operations.
type Block []Op

// ToStackOp is a stack operation that reads a value from the op-stack.
type ToStackOp struct {
	ToVar string
	Pos   int
	Type  LuaType
}

// CastToGoTypeOp casts a variable from a LuaType to a GoType.
// Self generating
type CastToGoTypeOp struct {
	ToVar   string
	FromVar string
	GoType  binds.Type
}

// PushStackOp is a stack operation that pushes a value onto the op-stack.
type PushStackOp struct {
	FromVar string
	Type    LuaType

	// True if the type must be cast
	MustCast bool
}

// CallFunctionOp calls a function
type CallNativeFunctionOp struct {
	Name    string
	ArgVars []string
	RetVars []string
}

// ReturnIntOp returns an integer result
type ReturnIntOp struct {
	IntVal int
}

// LuaType represents a native lua type
type LuaType int

const (
	NilLuaType LuaType = iota
	StringLuaType
	IntLuaType
	BoolLuaType
	Float32LuaType
	Float64LuaType
)

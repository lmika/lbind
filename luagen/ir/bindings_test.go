package ir

import (
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/lmika/lbind/parser"
	"github.com/seanpont/assert"
)

func TestBindings(t *testing.T) {
	assert := assert.Assert(t)

	b, err := parseBindings(`package test module test { func test(x: int): int = doTest }`)
	assert.Nil(err)
	assert.NotNil(b)

	module := b.Modules[0]
	assert.NotNil(module)

	fn := module.BindFns[0]
	assert.NotNil(fn)

	/* TODO
	assert.Equal(fn.Ops, []Op{
		ToStackOp{ToVar: "x", Pos: 1, Type: IntLuaType},
		CallNativeFunctionOp{Name: "doTest", ArgVars: []string{"x"}, RetVars: []string{"ret0"}},
		PushStackOp{FromVar: "ret0", Type: IntLuaType},
		ReturnIntOp{IntVal: 1},
	})
	*/
}

func parseBindings(def string) (*Bindings, error) {
	b, err := parser.Parse(strings.NewReader(def))
	if err != nil {
		return nil, fmt.Errorf("Parse error: %v", err)
	}

	return Generate(b)
}

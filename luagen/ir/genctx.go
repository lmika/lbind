package ir

import (
	"fmt"

	"github.com/dave/jennifer/jen"
)

// GenCtx holds a context for generation.  This can
// be used to generate temporary variable names, etc.
type GenCtx struct {
	// Group for this gen context
	G *jen.Statement

	// Custom function used for generating a new variable.  If not set, the GenCtx
	// will internally create new variables.
	NewVarFn func() string

	varIndex int
}

// NewVar creates and returns a new variable name.
func (gc *GenCtx) NewVar() string {
	if gc.NewVarFn != nil {
		return gc.NewVarFn()
	}

	gc.varIndex++
	return fmt.Sprintf("x%d", gc.varIndex)
}

package ir

import (
	"bitbucket.org/lmika/lbind/binds"
)

// Bindings contain all the modules.
type Bindings struct {
	Src     *binds.Bindings
	Modules []*Module
}

// Generate generates IR bindings from the binds bindings.
func Generate(binds *binds.Bindings) (*Bindings, error) {
	bindings := &Bindings{Src: binds}

	for _, module := range binds.Modules {
		if err := bindings.addModule(module); err != nil {
			return nil, err
		}
	}

	return bindings, nil
}

// addModule adds a module to the bindings
func (b *Bindings) addModule(module *binds.Module) error {
	m := &Module{Src: module}

	if err := m.addFunctions(module.Functions); err != nil {
		return err
	}

	b.Modules = append(b.Modules, m)
	return nil
}

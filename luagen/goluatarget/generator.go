package goluatarget

import (
	"bitbucket.org/lmika/lbind/luagen/ir"
	"github.com/dave/jennifer/jen"
	"github.com/pkg/errors"
)

// Target is a generator for the golua target
type Target struct {
	// CtxVar name of the context variable
	CtxVar string

	// Information regarding native types
	TypeInfo map[ir.LuaType]NativeTypeInfo
}

type NativeTypeInfo struct {
	GoTypeName   string
	PushStackFn  string
	ToStackFn    string
	TypeCastTo   string
	TypeCastPush string
}

// New creates a
func New() *Target {
	return &Target{
		CtxVar: "l",
		TypeInfo: map[ir.LuaType]NativeTypeInfo{
			ir.StringLuaType:  {GoTypeName: "string", PushStackFn: "PushString", ToStackFn: "ToString"},
			ir.IntLuaType:     {GoTypeName: "int", PushStackFn: "PushInteger", ToStackFn: "ToInteger"},
			ir.BoolLuaType:    {GoTypeName: "bool", PushStackFn: "PushBoolean", ToStackFn: "ToBoolean"},
			ir.Float32LuaType: {GoTypeName: "float32", PushStackFn: "PushNumber", ToStackFn: "ToNumber", TypeCastTo: "float32", TypeCastPush: "float64"},
			ir.Float64LuaType: {GoTypeName: "float64", PushStackFn: "PushNumber", ToStackFn: "ToNumber"},
		},
	}
}

func (t *Target) Generate(op ir.Op, gc *ir.GenCtx) error {
	switch o := op.(type) {
	case ir.Block:
		for _, b := range o {
			if err := t.Generate(b, gc); err != nil {
				return err
			}
			gc.G.Line()
		}
	case ir.ToStackOp:
		ti, hasTi := t.TypeInfo[o.Type]
		if !hasTi {
			return errors.Errorf("No information for type: %v", o.Type)
		}

		if ti.TypeCastTo != "" {
			varName := gc.NewVar()
			gc.G.Id(varName).Op(",").Id("_").Op(":=").Id(t.CtxVar).Dot(ti.ToStackFn).Call(jen.Lit(o.Pos)).Line()
			gc.G.Id(o.ToVar).Op(":=").Id(ti.TypeCastTo).Call(jen.Id(varName))
		} else {
			gc.G.Id(o.ToVar).Op(",").Id("_").Op(":=").Id(t.CtxVar).Dot(ti.ToStackFn).Call(jen.Lit(o.Pos))
		}
	case ir.PushStackOp:
		ti, hasTi := t.TypeInfo[o.Type]
		if !hasTi {
			return errors.Errorf("No information for type: %v", o.Type)
		}

		if ti.TypeCastPush != "" {
			gc.G.Id(t.CtxVar).Dot(ti.PushStackFn).Call(jen.Id(ti.TypeCastPush).Call(jen.Id(o.FromVar)))
		} else if o.MustCast {
			gc.G.Id(t.CtxVar).Dot(ti.PushStackFn).Call(jen.Id(ti.GoTypeName).Call(jen.Id(o.FromVar)))
		} else {
			gc.G.Id(t.CtxVar).Dot(ti.PushStackFn).Call(jen.Id(o.FromVar))
		}

	case ir.CastToGoTypeOp:
		gc.G.Id(o.ToVar).Op(":=").Add(o.GoType.ToJenId()).Call(jen.Id(o.FromVar))
	}

	return nil
}
